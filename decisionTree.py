# Decision Tree Classifier


from sklearn import datasets
from sklearn import metrics
import numpy as np
from sklearn.tree import DecisionTreeClassifier
from sys import argv
import math

def dt(filename, testName, label, xstart):
    print " Classification and Regression Trees (CART) decision tree algo Demo, version 1.0"

    # input dataset
    # if (len(argv) < 2):
    #     raise NameError('No filename specified')

    print "Loading Data..."

    # learning set
    # filename = argv[1]
    data = np.genfromtxt(filename, comments="#", delimiter=',', autostrip=True, skip_header=1)
    (height, width) = data.shape

    y_train = data[0:,label:label+1]
    X_train = data[0:,xstart:width] #input
    #print "Training set input data dimension is " + str(X_train.shape)

    # test set
    # testName = argv[2]
    data = np.genfromtxt(testName, comments="#", delimiter=',', autostrip=True, skip_header=1)
    (height, width) = data.shape

    y_valid = data[0:,label:label+1]
    X_valid = data[0:,xstart:width] #input
    print "------Beginning CART Run------"

    # fit a CART model to the data
    model = DecisionTreeClassifier()
    model.fit(X_train, y_train)
    # print(model)
    # make predictions
    print "###Accuracy for training set..."
    expected = y_train
    predicted = model.predict(X_train)
    print(metrics.classification_report(expected, predicted))
    print(metrics.confusion_matrix(expected, predicted))

    print "###Accuracy for validation set..."
    expected = y_valid
    predicted = model.predict(X_valid)
    # print predicted
    # summarize the fit of the model
    print(metrics.classification_report(expected, predicted))
    print(metrics.confusion_matrix(expected, predicted))
    print "###Calculating hit ratio..."
    sum = 0
    for i in xrange(len(y_valid)):
        if (y_valid[i] != predicted[i]):
            sum = sum + 1
    print "Hit ratio: "+str(1- sum * 1.0 /len(predicted))
    print("    score is %.3f" % model.score(X_valid, y_valid))
    print("    RMSE is %.3f" % math.sqrt(np.mean((predicted - y_valid) ** 2)))
    return predicted
