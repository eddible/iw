# imports the filename specified at command line
# outputs the basic multi-regression result
# toggle the info option by typing in '--info'

import pandas as pd
import numpy as np
import statsmodels.api as sm
from sys import argv

if (len(argv) < 2):
    raise NameError('No filename specified')

filename = argv[1]
#print filename
df_adv = pd.read_csv(filename, index_col=0)

X = df_adv[['TempChange','Nikkei225Change','DowJonesChange','CrudeChange','RMBChange','USDChange','GoldChange',
            'YenChange','YieldSpreadChange','NasdaqChange','BSE30Change','CACChange','DAXChange',
            'FTSEChange', 'TBillChange',
            'HSIChange','NYSEChange','Russell2000Change','SHChange']]

y = df_adv['SP_Change']

X = sm.add_constant(X)
est = sm.OLS(y, X).fit()

print est.summary()
if (len(argv) > 2):
    if (argv[2] == "--info"):
        print df_adv.info()
