from Tkinter import *
from tkFileDialog import askopenfilename
import ttk
import numpy as np

def calculate(*args):
    filename = askopenfilename()
    meters.set(filename)

def validation(*args):
    filename = askopenfilename()
    valid.set(filename)

def showcase(*args):
    f = meters.get()
    v = valid.get()
    data = np.genfromtxt(f, comments="#", delimiter=',', autostrip=True, skip_header=1)
    print data.shape
    #stage1main.main(f, v)

root = Tk()
root.title("Stage 1 File Picker")

mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)

label = StringVar()
xstart = StringVar()
meters = StringVar()
valid = StringVar()

label_entry = ttk.Entry(mainframe, width=7, textvariable=label)
ttk.Label(mainframe, text="Label is").grid(column=1, row=5, sticky=E)
label_entry.grid(column=2, row=5, sticky=(W, E))

xstart_entry = ttk.Entry(mainframe, width=7, textvariable=xstart)
ttk.Label(mainframe, text="X_Start is").grid(column=1, row=6, sticky=E)
xstart_entry.grid(column=2, row=6, sticky=(W, E))

ttk.Label(mainframe, textvariable=meters).grid(column=2, row=2, sticky=(W, E))
ttk.Button(mainframe, text="Pick Learning", command=calculate).grid(column=1, row=4, sticky=W)

ttk.Label(mainframe, textvariable=valid).grid(column=2, row=3, sticky=(W, E))
ttk.Button(mainframe, text="Pick Validation", command=validation).grid(column=2, row=4, sticky=W)

ttk.Label(mainframe, text="Learning").grid(column=3, row=2, sticky=W)
ttk.Label(mainframe, text="Learning file is").grid(column=1, row=2, sticky=E)
#ttk.Label(mainframe, text="---").grid(column=3, row=2, sticky=W)
ttk.Label(mainframe, text="Validation").grid(column=3, row=3, sticky=W)
ttk.Label(mainframe, text="Validation file is").grid(column=1, row=3, sticky=E)

ttk.Button(mainframe, text="Run", command=showcase).grid(column=3, row=4, sticky=W)

for child in mainframe.winfo_children(): child.grid_configure(padx=5, pady=5)

# feet_entry.focus()
# root.bind('<Return>', calculate)
# root.bind('<Return>', valid)

root.mainloop()
