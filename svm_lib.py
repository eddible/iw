# Eddie Chen, Feb 29, 2016
# SVM for classfication

# input two files, output SVM prediction

import numpy as np
from sys import argv
from sklearn import neighbors
from sklearn import svm
from sklearn import cluster, datasets
import math
import datetime
from sklearn import preprocessing
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.svm import SVC
from time import time
from operator import itemgetter
from scipy.stats import randint as sp_randint
import math

def svmlib(filename, testName, label, xstart):
    totalstart = time()
    #reference:
    print "SVM Demo, version 1.1 with new buckets"
    today = datetime.date.today()
    print today
    # input dataset
    # if (len(argv) < 2):
    #     raise NameError('No filename specified')

    print "Loading Data..."

    # learning set
    # filename = argv[1]
    data = np.genfromtxt(filename, comments="#", delimiter=',', autostrip=True, skip_header=1)
    (height, width) = data.shape

    y_train = data[0:,label:label+1]
    X_train = data[0:,xstart:width] #input
    scaler = preprocessing.StandardScaler().fit(X_train)
    X_train = scaler.transform(X_train)
    print "Training set input data dimension is " + str(X_train.shape)

    # test set
    # testName = argv[2]
    valid = np.genfromtxt(testName, comments="#", delimiter=',', autostrip=True, skip_header=1)

    y_valid = valid[0:,label:label+1]
    res = y_valid
    X_valid = valid[0:,xstart:width] #input
    X_valid = scaler.transform(X_valid)

    print "------Beginning SVM Run------"
    #
    #
    print "Setting up k-nearest neighbor classifier..."
    knn = neighbors.KNeighborsClassifier()
    print "default value = 5 works fine"
    knn.fit(X_train, y_train.ravel())
    y_valid_predict = knn.predict(X_valid)
    res = np.column_stack((res,y_valid_predict))
    #res = y_valid_predict;
    #check accuracy
    print "Done, checking KNN accuracy..."
    sum = 0
    for i in xrange(len(y_valid)):
        if (y_valid[i] != y_valid_predict[i]):
            sum = sum + 1

    print "Hit ratio: "+str(1- sum * 1.0 /len(y_valid_predict))
    print("    score is %.3f" % knn.score(X_valid, y_valid))
    print("    RMSE is %.3f" % math.sqrt(np.mean((y_valid_predict - y_valid) ** 2)))
    print ""
    ### this version is much faster
    print "Setting up linear SVM ver2..."
    svc = svm.LinearSVC()
    svc.fit(X_train, y_train.ravel())
    y_valid_predict = svc.predict(X_valid)
    res = np.column_stack((res,y_valid_predict))
    #np.append(res,y_valid_predict,axis = 1)
    print "Done, checking linear SVM ver2 accuracy..."
    sum = 0
    for i in xrange(len(y_valid)):
        if (y_valid[i] != y_valid_predict[i]):
            sum = sum + 1

    print "Hit ratio: "+str(1- sum * 1.0 /len(y_valid_predict))
    print("    score is %.3f" % svc.score(X_valid, y_valid))
    print("    RMSE is %.3f" % math.sqrt(np.mean((y_valid_predict - y_valid) ** 2)))
    print ""
    ### didn't finish, taking too long
    print "Setting up poly-kernel SVM..."
    svc = svm.SVC(kernel='poly')
    svc.fit(X_train, y_train.ravel())
    y_valid_predict = svc.predict(X_valid)
    res = np.column_stack((res,y_valid_predict))
    print "Done, checking Ploy-kernel SVM accuracy..."
    sum = 0
    for i in xrange(len(y_valid)):
        if (y_valid[i] != y_valid_predict[i]):
            sum = sum + 1

    print "Hit ratio: "+str(1.0-sum * 1.0/len(y_valid_predict))
    print("    score is %.3f" % svc.score(X_valid, y_valid))
    print("    RMSE is %.3f" % math.sqrt(np.mean((y_valid_predict - y_valid) ** 2)))
    print ""
    print "Setting up RBF-kernel SVM..."
    svc = svm.SVC(kernel='rbf')
    svc.fit(X_train, y_train.ravel())
    y_valid_predict = svc.predict(X_valid)
    res = np.column_stack((res,y_valid_predict))
    print "Parameters for this predictor are:"
    print svc.get_params()
    #np.append(res,y_valid_predict,axis = 1)
    print "Done, checking RBF-kernel SVM accuracy..."
    sum = 0
    for i in xrange(len(y_valid)):
        if (y_valid[i] != y_valid_predict[i]):
            sum = sum + 1

    print "Hit ratio: "+str(1- sum * 1.0/len(y_valid_predict))
    print("    score is %.3f" % svc.score(X_valid, y_valid))
    print("    RMSE is %.3f" % math.sqrt(np.mean((y_valid_predict - y_valid) ** 2)))
    print ""

    print "Setting up k-means clustering..."
    n_cls = len(np.unique(y_train))
    print "In this run, it's %d-means" % n_cls
    k_means = cluster.KMeans(n_clusters=n_cls)
    k_means.fit(X_train)
    y_valid_predict = k_means.fit_predict(X_valid)
    res = np.column_stack((res,y_valid_predict))
    #print y_valid_predict
    print "Done, checking k-means clustering accuracy..."
    sum = 0
    for i in xrange(len(y_valid)):
        if (y_valid[i] != y_valid_predict[i]):
            sum = sum + 1
    print "Hit ratio: "+str(1 - sum * 1.0/len(y_valid_predict))
    print("    score is %.3f" % k_means.score(X_valid, y_valid))
    print("    RMSE is %.3f" % math.sqrt(np.mean((y_valid_predict - y_valid) ** 2)))
    ## about 51.8% accuracy rate
    # use for sanity check
    print ""
    # run grid search for best parameter settings
    y_train = y_train.ravel()
    print "Running Grid Search for SVM Parameters (C, kernel, gamma)"
    print "Testing rbf /linear /polynomial /sigmoid kernels"
    # tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
    #                  'C': [1, 10, 100, 1000]},
    #                 {'kernel': ['linear'], 'C': [1, 10, 100, 1000]},
    #                 {'kernel': ['poly'], 'C': [1, 10, 100, 1000]},
    #                 {'kernel': ['sigmoid'], 'C': [1, 10, 100, 1000]}
    #                 ]
    tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                     'C': [1, 10, 100, 1000]},
                     {'kernel': ['linear'], 'C': [1, 10]},
                     {'kernel': ['poly']},
                     {'kernel': ['sigmoid'], 'C':[1,10]}] # for testing purpose
    svc = svm.SVC()
    print "......Searching......"
    start = time()
    clf = GridSearchCV(svc, tuned_parameters, n_jobs=-1, cv=5)
    clf.fit(X_train, y_train)
    print ""
    print("GridSearchCV took %.2f seconds for %d candidate parameter settings."
      % (time() - start, len(clf.grid_scores_)))
    report(clf.grid_scores_)
    print("Best parameters set found on the training set:")
    print(clf.best_params_)

    y_valid_predict = clf.predict(X_valid)
    print "/------------------------------/"
    print "Done, checking SVM accuracy..."
    sum = 0
    for i in xrange(len(y_valid)):
        if (y_valid[i] != y_valid_predict[i]):
            sum = sum + 1
    print "Hit ratio: "+str(1 - sum * 1.0/len(y_valid_predict))
    print("    score is %.3f" % clf.score(X_valid, y_valid))
    print("    RMSE is %.3f" % math.sqrt(np.mean((y_valid_predict - y_valid) ** 2)))
    res = np.column_stack((res,y_valid_predict))

    print ""
    print "Total time taken: %.2f seconds" % (time() - totalstart)
    print ""
    print "------End of SVM Run------"
    return res

# Utility function to report best scores
def report(grid_scores, n_top=3):
    top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        print("Model with rank: {0}".format(i + 1))
        print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
              score.mean_validation_score,
              np.std(score.cv_validation_scores)))
        print("Parameters: {0}".format(score.parameters))
        print("")

def test_main():
    #test main
    filename = 'Master_Color_Learning_0304.csv'
    testName = 'Master_Color_Test_0304.csv'
    label = 12
    xstart = 15
    svmlib(filename, testName, label, xstart)

#test_main()
