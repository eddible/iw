import pandas as pd
import numpy as np
import statsmodels.api as sm
from sys import argv

if (len(argv) < 2):
    raise NameError('No filename specified')

filename = argv[1]
#print filename
df = pd.read_csv(filename, index_col=0)
y = df['SP_Change']
# y = df['CloseYesterday']
X = df['5y5yForward']

# print df.info()

X = sm.add_constant(X)
est = sm.OLS(y, X).fit()

print est.summary()
if (len(argv) > 2):
    if (argv[2] == "--info"):
        print df.info()