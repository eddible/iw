# Stage II, chaos-based firefly algo with SVR
# Eddie Chen '16, March 11, 2016

# references: https://msdn.microsoft.com/en-us/magazine/mt147244.aspx
"""
 Reference Paper
I. Fister Jr.,  X.-S. Yang,  I. Fister, J. Brest, Memetic firefly algorithm for combinatorial optimization,
in Bioinspired Optimization Methods and their Applications (BIOMA 2012), B. Filipic and J.Silc, Eds.
Jozef Stefan Institute, Ljubljana, Slovenia, 2012

Kazem, Ahmad, et al. "Support vector regression with chaos-based firefly algorithm
for stock market price forecasting." Applied soft computing 13.2 (2013): 947-958.

"""
#log mapping for chaotic mapping operator
def CMO(x):
    mu = 0.4
    return x*mu*(1-x)





"""
Main
"""
# constants from Kazem paper
n = 20		# number of fireflies
MaxGeneration = 200		# number of iterations
absorb = 1 #absorption constant
annealing = 0.25
beta0 = 4
