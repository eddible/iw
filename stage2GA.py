# Stage II, genetic algorithm
# Eddie Chen '16, March 11, 2016

# references: http://lethain.com/genetic-algorithms-cool-name-damn-simple/
# Choudhry, Rohit, and Kumkum Garg. "A hybrid machine learning system for stock
# market forecasting." World Academy of Science, Engineering and Technology
# 39.3 (2008): 315-318.

# A Genetic Algorithm is now used to select a set of salient features from among them.
# The selected features are used as inputs to a Support Vector Machine. The purpose
# here is to obtain an optimal subset of features which produce the best possible results.

# use a binary array to code if a feature is selected a not

""" You can select different functions for fitness, choices include:
    SVR (Linear, Poly or RBF Kernel), Linear Regression (R^2)

    Parameters for evolution:
    retain=0.2, random_select=0.05, mutate=0.1

    Population count: 100 (this directly impacts the running speed)
    Generation count: 10 (this directly impacts the running speed)
"""
from random import randint, random
from sys import argv
import numpy as np
import pandas as pd
from sklearn import svm, preprocessing
from operator import add
import math
from sklearn.cross_validation import cross_val_predict
from sklearn import linear_model
from progress.bar import Bar

def individual(size):
    'Create a member of the population.' # create individual chromosome
    return [ randint(0,1) for x in xrange(size) ]

def population(count, size):
    """
    Create a number of individuals (i.e. a population).
    """
    return [ individual(size) for x in xrange(count) ]

def fitness(individual, data, valid):
    """
    Determine the fitness of an individual. smaller is better.
    individual: the individual to evaluate
    target: the target number individuals are aiming for
    """
    #err = svr_label(individual, data, valid) #run SVR to determine accuracy
    err = linearReg(individual, data, valid)
    return err #smaller, better

def grade(pop, data, valid):
    'Find average fitness for a population.'
    # summed = reduce(add, (fitness(x, data, valid, label, xstart) for x in pop))
    # return summed / (len(pop) * 1.0)
    summed = 0;
    for x in pop:
        summed = summed + fitness(x, data, valid)
    #return summed / (len(pop) * 1.0)
    return summed

def evolve(pop, data, valid, retain=0.2, random_select=0.05, mutate=0.1):
    graded = [ (fitness(x, data, valid), x) for x in pop]
    graded = [ x[1] for x in sorted(graded)]
    ### Temporarily print top child
    # print "Top child is ", graded[0]

    retain_length = int(len(graded)*retain)
    parents = graded[:retain_length]
    # randomly add other individuals to
    # promote genetic diversity
    for individual in graded[retain_length:]:
        if random_select > random():
            parents.append(individual)
    #print "after random select, parents#: ", len(parents)
    #print "parents are", parents
    # mutate some individuals
    for individual in parents:
        if mutate > random():
            pos_to_mutate = randint(0, len(individual)-1)
            individual[pos_to_mutate] = randint(
                0, 1)
    #print "after mutation..."
    # crossover parents to create children
    parents_length = len(parents)
    desired_length = len(pop) - parents_length
    children = []
    #print "desired_length is ", desired_length
    while len(children) < desired_length:
        male = randint(0, parents_length-1)
        female = randint(0, parents_length-1)
        if male != female:
            male = parents[male]
            female = parents[female]

            half = len(male) / 2 # use half of male and half of female;
            child = male[:half] + female[half:]
            #TODO: consider dominant versus recessive? figure out how it works
            # child = male;
            # for i in xrange(len(male)):
            #     if ((male[i] + female[i]) >= 1):
            #         child[i] = 1
            #     else:
            #         child[i] = 0
            children.append(child)
    parents.extend(children)
    return parents

def svr_label(individual, data, valid):
    """
    svr to calculate the error rate; data and valid are both ndarrays
    """
    sel = individual;
    #print("individual is ", sel);
    (h, width) = data.shape
    all_data = data[0:,1:width]
    newdata = data[0:,0:1]
    for x in xrange(len(sel)):
        if (sel[x] == 1):
            newdata = np.hstack((newdata, all_data[0:,x:x+1]))
    #print newdata.shape
    (dim, wid) = newdata.shape
    if (wid==1):
        return 100000 #to penalize 0
    # y_train = data[0:,label:label+1]
    # X_train = data[0:,xstart:width] #input
    y_train = newdata[0:, 0:1]
    X_train = newdata[0:,1:wid]
    # do the same for Validation
    all_data = valid[0:,1:width]
    newdata = valid[0:,0:1]
    for x in xrange(len(sel)):
        if (sel[x] == 1):
            newdata = np.hstack((newdata, all_data[0:,x:x+1]))

    (dim, wid) = newdata.shape
    y_valid = newdata[0:, 0:1]
    X_valid = newdata[0:,1:wid]

    # scalerX = preprocessing.StandardScaler().fit(X_train)
    # X_train = scalerX.transform(X_train)
    # X_valid = scalerX.transform(X_valid)
    # scalerY = preprocessing.StandardScaler().fit(y_train)
    # y_train = scalerY.transform(y_train)
    # y_valid = scalerY.transform(y_valid)

    #svc = svm.SVR(kernel='rbf', C=10.0, gamma=10.0) #params based on gridsearch
    #svc = svm.SVR(kernel='rbf', C=10.0, gamma=10.0)
    svc = svm.SVR()
    #print X_train.shape
    svc.fit(X_train, y_train.ravel())
    y_valid_predict = svc.predict(X_valid)
    score = svc.score(X_valid, y_valid.ravel())
    sum = 0
    for i in xrange(len(y_valid)):
        sum = sum + pow(abs(y_valid_predict[i] - y_valid[i]),2)

    return math.sqrt(sum) ##bigger the worse
    #return score

def print_label(individual, filename, xstart):
    dd = pd.read_csv(filename)
    print "Selected columns' name and index are as follows: "
    for iter in range(len(dd.columns) - xstart):
        if (individual[iter] == 1):
            print(dd.columns[iter + xstart],iter+xstart)

def linearReg(individual, data, valid):
    """
    linear regression to calculate the fitness score;
    data and valid are both ndarrays
    """
    sel = individual;
    (h, width) = data.shape
    all_data = data[0:,1:width]
    newdata = data[0:,0:1]
    for x in xrange(len(sel)):
        if (sel[x] == 1):
            newdata = np.hstack((newdata, all_data[0:,x:x+1]))
    #print newdata.shape
    (dim, wid) = newdata.shape
    # y_train = data[0:,label:label+1]
    # X_train = data[0:,xstart:width] #input
    y_train = newdata[0:, 0:1]
    X_train = newdata[0:,1:wid]
    # do the same for Validation
    all_data = valid[0:,1:width]
    newdata = valid[0:,0:1]
    for x in xrange(len(sel)):
        if (sel[x] == 1):
            newdata = np.hstack((newdata, all_data[0:,x:x+1]))

    (dim, wid) = newdata.shape
    y_valid = newdata[0:, 0:1]
    X_valid = newdata[0:,1:wid]

    clf = linear_model.LinearRegression()
    # Train the model using the training sets
    clf.fit(X_train, y_train)
    # # The coefficients
    # print('Coefficients: \n', clf.coef_)
    # # The mean square error
    # print("Residual sum of squares: %.2f"
    #   % np.mean((clf.predict(X_valid) - y_valid) ** 2))
    # # Explained variance score: 1 is perfect prediction
    # print('Variance score: %.2f' % clf.score(X_valid, y_valid))
    return np.mean((clf.predict(X_valid) - y_valid) ** 2)

def GA(data, valid):
    """
        Main, TODO: automate parameter input
    """
    # # input dataset
    # print "Loading Data..."
    # # input dataset
    # if (len(argv) < 2):
    #     raise NameError('No filename/not enough filenames specified')
    #
    # filename = argv[1]
    # data = np.genfromtxt(filename, comments="#", delimiter=',', autostrip=True, skip_header=1)
    # (height, width) = data.shape
    # print "data dimension is "+str(data.shape)
    # # dd = pd.read_csv(filename)
    # # print "for your reference, columns' name and index are as follows: "
    # # for iter in range(len(dd.columns)):
    # #     print(dd.columns[iter],iter)
    # # toggle column number
    # # label = int(raw_input('y variable column: '))
    # # xstart = int(raw_input('X variable column start: '))
    # #
    # # print "Selected y as "+str(label)
    # # print "Selected x starts at "+str(xstart)
    # label = 3
    # xstart = 4;
    # # test set
    # testName = argv[2]
    # #
    # # print('Learning set is ', filename)
    # #
    # # print('Validation set is ', testName)
    # size = width - xstart
    #
    # valid = np.genfromtxt(testName, comments="#", delimiter=',', autostrip=True, skip_header=1)
    # # imp = preprocessing.Imputer(missing_values='NaN', strategy='mean', axis=0)
    # # print(imp.transform(data))
    data = preprocessing.Imputer().fit_transform(data)
    valid = preprocessing.Imputer().fit_transform(valid)
    (height, size) = data.shape
    # print np.all(np.isfinite(data))
    #scaler = preprocessing.StandardScaler().fit(data)
    #data = scaler.transform(data)
    #valid = scaler.transform(valid)

    population_count = 100 #TODO: get this value from input
    generations = 100 #number of generations to run

    p = population(population_count, size)
    # print "Top child is ", p[0]
    fitness_history = [grade(p, data, valid),]

    bar = Bar('Processing', max=generations, suffix='%(percent)d%%') #progress bar to print progress

    for i in xrange(generations):
        p = evolve(p, data, valid)
        # if (i % 100 == 0):
        #     print "top child is, ", p[0]
        fitness_history.append(grade(p, data, valid))
        bar.next()

    bar.finish()
    # for datum in fitness_history:
    #    print datum
    filename = 'Master_Change_Learning_0304.csv'
    print_label(p[0], filename, 4)

    """
        print out the SVR prediction
    """
    print "Piping into the SVR..."
    sel = p[0];
    #print("individual is ", sel);

    all_data = data[0:,1:size]
    newdata = data[0:,0:1]
    for x in xrange(len(sel)):
        if (sel[x] == 1):
            newdata = np.hstack((newdata, all_data[0:,x:x+1]))
    print newdata.shape
    (dim, wid) = newdata.shape
    y_train = newdata[0:, 0:1]
    X_train = newdata[0:,1:wid]
    # do the same for Validation
    all_data = valid[0:,1:size]
    newdata = valid[0:,0:1]
    for x in xrange(len(sel)):
        if (sel[x] == 1):
            newdata = np.hstack((newdata, all_data[0:,x:x+1]))
    print newdata.shape
    (dim, wid) = newdata.shape
    y_valid = newdata[0:, 0:1]
    X_valid = newdata[0:,1:wid]

    #svc = svm.SVR(kernel='rbf', C=1e1, gamma=1) #we can modify these params, see gridsearch
    svc = svm.SVR()
    #print X_train.shape
    svc.fit(X_train, y_train.ravel())
    y_valid_predict = svc.predict(X_valid)

    # The mean square error
    print("Root Mean Square Error: %.2f"% math.sqrt(np.mean((y_valid_predict - y_valid) ** 2)))
    # Explained variance score: 1 is perfect prediction
    print "Explained variance score: 1 is perfect prediction"
    print('Variance score: %.2f' % svc.score(X_valid, y_valid))
    #print "error term is ", np.mean((y_valid_predict - y_valid) ** 2)
    return y_valid_predict
