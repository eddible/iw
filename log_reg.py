### Eddie Chen '16
### Logistic regression model

from sklearn.linear_model import LogisticRegression
from sklearn import linear_model
import numpy as np
from sys import argv
from sklearn import preprocessing
import math

def logreg(filename, testName, label, xstart):
    print "Log Regression Demo, version 1.0"

    # input dataset
    # if (len(argv) < 2):
    #     raise NameError('No filename specified')

    print "Loading Data..."

    # learning set
    #filename = argv[1]
    data = np.genfromtxt(filename, comments="#", delimiter=',', autostrip=True, skip_header=1)
    (height, width) = data.shape

    # spCol = 5
    # spDailyChange = 8
    # upordown = 14
    # bucket = 12
    # shiller = 15
    # # toggle column number
    # label = bucket

    y_train = data[0:,label:label+1]
    X_train = data[0:,xstart:width] #input
    print "Training set input data dimension is " + str(X_train.shape)

    # test set
    # testName = argv[2]
    data = np.genfromtxt(testName, comments="#", delimiter=',', autostrip=True, skip_header=1)
    (height, width) = data.shape

    y_valid = data[0:,label:label+1]
    X_valid = data[0:,xstart:width] #input
    print "------Beginning Logistic Regression Run------"
    #
    #
    print "Setting up log regression model classifier..."
    log_reg = LogisticRegression().fit(X_train, y_train.ravel())
    y_valid_predict = log_reg.predict(X_valid)

    #check accuracy
    print "Done, checking log reg model accuracy..."
    sum = 0
    for i in xrange(len(y_valid)):
        if (y_valid[i] != y_valid_predict[i]):
            sum = sum + 1

    print "Hit ratio: "+str(1- sum * 1.0 /len(y_valid_predict))
    print("    score is %.3f" % log_reg.score(X_valid, y_valid))
    print("    RMSE is %.3f" % math.sqrt(np.mean((y_valid_predict - y_valid) ** 2)))
    print "/*******************************/"
    return y_valid_predict

def SGDClassifier(filename, testName, label, xstart):
    # SGDClassifier, with loss function specified as log
    # reference: http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.SGDClassifier.html
    print "SGDClassifier Demo, version 1.0"
    print "Loading Data..."

    data = np.genfromtxt(filename, comments="#", delimiter=',', autostrip=True, skip_header=1)
    (height, width) = data.shape

    y_train = data[0:,label:label+1]
    X_train = data[0:,xstart:width] #input
    print "Training set input data dimension is " + str(X_train.shape)

    data = np.genfromtxt(testName, comments="#", delimiter=',', autostrip=True, skip_header=1)
    (height, width) = data.shape

    y_valid = data[0:,label:label+1]
    X_valid = data[0:,xstart:width] #input
    print "------Beginning SGDClassifier Run------"

    print "Setting up stochastic gradient descent classifier..."
    sgd = linear_model.SGDClassifier(loss='log')
    sgd.fit(X_train, y_train.ravel())
    print sgd
    y_valid_predict = sgd.predict(X_valid)

    #check accuracy
    print "Done, checking SGDClassifier accuracy..."
    sum = 0
    for i in xrange(len(y_valid)):
        if (y_valid[i] != y_valid_predict[i]):
            sum = sum + 1

    print "Hit ratio: "+str(1- sum * 1.0 /len(y_valid_predict))
    print("    score is %.3f" % sgd.score(X_valid, y_valid))
    print("    RMSE is %.3f" % math.sqrt(np.mean((y_valid_predict - y_valid) ** 2)))
    print "/*******************************/"
    return y_valid_predict



# test main
# label = 12
# xstart = 15
# print SGDClassifier(argv[1], argv[2], label, xstart)
