### Support Vector Regression
from sys import argv
from sklearn import svm
import numpy as np
import math
from sklearn import preprocessing
import stage2plot
import bucketing

def svr(filename, testName, label, xstart):
    print "Loading Data..."

    # learning set
    # filename = argv[1]
    data = np.genfromtxt(filename, comments="#", delimiter=',', autostrip=True, skip_header=1)
    (height, width) = data.shape
    data = preprocessing.Imputer().fit_transform(data)


    y_train = data[0:,label:label+1]
    X_train = data[0:,xstart:width] #input
    scaler = preprocessing.StandardScaler().fit(X_train)
    X_train = scaler.transform(X_train)
    scalerY = preprocessing.StandardScaler().fit(y_train)
    y_train = scalerY.transform(y_train)
    print "Training set input data dimension is " + str(X_train.shape)

    data = np.genfromtxt(testName, comments="#", delimiter=',', autostrip=True, skip_header=1)
    data = preprocessing.Imputer().fit_transform(data)
    (height, width) = data.shape

    y_valid = data[0:,label:label+1]
    X_valid = data[0:,xstart:width] #input
    X_valid = scaler.transform(X_valid)
    y_valid = scalerY.transform(y_valid)

    print "------Beginning SVR Run------"
    print "Setting up SVR..."
    svc = svm.SVR()
    svc.fit(X_train, y_train.ravel())
    y_valid_predict = svc.predict(X_valid)
    score = svc.score(X_valid, y_valid)
    # y_valid_predict = scalerY.inverse_transform(y_valid_predict)
    # y_valid = scalerY.inverse_transform(y_valid)
    print "Score is " + str(score)
    print "Applying inverse_transform by scalerY..."
    y_valid_predict = scalerY.inverse_transform(y_valid_predict)
    y_valid = scalerY.inverse_transform(y_valid)
    print "Done, checking SVR accuracy..."
    print "Root mean sqaure error is: "+str(math.sqrt(np.mean((y_valid_predict - y_valid) ** 2)))
    #stage2plot.plot(y_valid_predict, y_valid)
    print "Discretizing..."
    mean = np.mean(y_valid_predict)
    std =np.std(y_valid_predict)
    res_svr = bucketing.bucketing(y_valid_predict, mean, std)
    res_valid = bucketing.bucketing(y_valid, np.mean(y_valid), np.std(y_valid))
    print "Calculating hit ratio..."
    #print len(res_valid)
    sum = 0
    for i in xrange(len(res_valid)):
        if (res_svr[i] != res_valid[i]):
            sum = sum + 1
    print "Hit ratio: "+str(1- sum * 1.0 /len(res_valid))
    print "/*******************************/"
    #return y_valid_predict
    return res_svr

def test_main():
    filename = 'Master_Color_Learning_0304.csv'
    testName = 'Master_Color_Test_0304.csv'
    label = 5
    xstart = 15
    svr(filename, testName, label, xstart)

#test_main()
