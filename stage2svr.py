# Stage II, optimized SVR and kernel ridge regression
# kernel trick

# Eddie Chen '16, March 13, 2016

# references: http://scikit-learn.org/stable/auto_examples/plot_kernel_ridge_regression.html

"""
    Both kernel ridge regression (KRR) and SVR learn a non-linear function by
    employing the kernel trick, i.e., they learn a linear function in the space
    induced by the respective kernel which corresponds to a non-linear function
    in the original space. They differ in the loss functions
    (ridge versus epsilon-insensitive loss). In contrast to SVR, fitting a KRR
    can be done in closed-form and is typically faster for medium-sized datasets.
    On the other hand, the learned model is non-sparse and thus slower than SVR
    at prediction-time.
"""
import time
import math
import numpy as np
from sys import argv
import pandas as pd
from sklearn import preprocessing
from sklearn.svm import SVR
from sklearn.grid_search import GridSearchCV
from sklearn.learning_curve import learning_curve
from sklearn.kernel_ridge import KernelRidge
import matplotlib.pyplot as plt
from operator import itemgetter
from scipy.stats import randint as sp_randint

def svr_search(X_train, y_train, X_valid, y_valid):
    print "=============== Running Grid Search SVR ==============="
    print "Training X dataset dimension is", X_train.shape
    print "Searching..."
    # fit regression Model
    #TODO: expand search space?
    # remove the CV param to use default 3-fold strategy
    svr = GridSearchCV(SVR(kernel='rbf', gamma=0.1),
                       param_grid={"C": [1e0, 1e1, 1e2, 1e3],
                                   "gamma": np.logspace(-2, 2, 5)})
    # results
    t0 = time.time()
    svr.fit(X_train, y_train.ravel())
    svr_fit = time.time() - t0
    print("SVR complexity and bandwidth selected and model fitted in %.3f s"
          % svr_fit)

    sv_ratio = svr.best_estimator_.support_.shape[0] / (len(y_train) * 1.0)
    print("Support vector ratio: %.3f" % sv_ratio)

    t0 = time.time()
    y_svr = svr.predict(X_valid)
    svr_predict = time.time() - t0
    print("SVR prediction for %d inputs in %.3f s"
          % (X_valid.shape[0], svr_predict))
    print ""
    report(svr.grid_scores_)
    print("Best parameters set found on the training set:")
    print(svr.best_params_)
    #print("Optimized SVR has prediction score of %.3f" % svr.score(X_valid, y_valid))
    print("Optimized SVR has RMSE of %.3f" % math.sqrt(np.mean((y_svr - y_valid) ** 2)))
    print("Optimized SVR has score of %.3f" % svr.score(X_valid, y_valid))
    print ""
    return y_svr

def kernel_ridge(X_train, y_train, X_valid, y_valid):
    print "=============== Running Kernel Ridge SVR ==============="
    kr = GridSearchCV(KernelRidge(kernel='rbf', gamma=0.1),
                      param_grid={"alpha": [1e0, 0.1, 1e-2, 1e-3,1e-4],
                                  "gamma": np.logspace(-2, 2, 5)})
    print "Searching..."
    # results
    t0 = time.time()
    kr.fit(X_train, y_train.ravel())
    kr_fit = time.time() - t0
    print("KRR complexity and bandwidth selected and model fitted in %.3f s"
          % kr_fit)
    t0 = time.time()
    y_kr = kr.predict(X_valid)
    kr_predict = time.time() - t0
    print("KRR prediction for %d inputs in %.3f s"
          % (X_valid.shape[0], kr_predict))
    print ""
    report(kr.grid_scores_)
    print("Best parameters set found on the training set:")
    print(kr.best_params_)
    print("Kernel Ridge Regression has RMSE of %.3f" % math.sqrt(np.mean((y_kr - y_valid) ** 2)))
    print("KRR SVR has score of %.3f" % kr.score(X_valid, y_valid))
    print ""
    return y_kr

# Utility function to report best scores
def report(grid_scores, n_top=3):
    top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        print("Model with rank: {0}".format(i + 1))
        print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
              score.mean_validation_score,
              np.std(score.cv_validation_scores)))
        print("Parameters: {0}".format(score.parameters))
        print("")

def test_main(a,b):
    filename = a
    data = np.genfromtxt(filename, comments="#", delimiter=',', autostrip=True, skip_header=1)
    (height, width) = data.shape
    print "data dimension is "+str(data.shape)
    # dd = pd.read_csv(filename)
    # print "for your reference, columns' name and index are as follows: "
    # for iter in range(len(dd.columns)):
    #     print(dd.columns[iter],iter)
    # toggle column number
    # label = int(raw_input('y variable column: '))
    # xstart = int(raw_input('X variable column start: '))
    #
    # print "Selected y as "+str(label)
    # print "Selected x starts at "+str(xstart)
    label = 3
    xstart = 4;
    # test set
    testName = b
    valid = np.genfromtxt(testName, comments="#", delimiter=',', autostrip=True, skip_header=1)
    #
    # print('Learning set is ', filename)
    #
    # print('Validation set is ', testName)
    size = width - xstart

    # standardize data
    data = preprocessing.Imputer().fit_transform(data)
    valid = preprocessing.Imputer().fit_transform(valid)
    scaler = preprocessing.StandardScaler().fit(data)
    data = scaler.transform(data)
    valid = scaler.transform(valid)
    # extract X and y
    y_train = data[0:,label:label+1]
    X_train = data[0:,xstart:width] #input

    y_valid = valid[0:,label:label+1]
    X_valid = valid[0:,xstart:width] #input
    svr_search(X_train, y_train, X_valid, y_valid)
    kernel_ridge(X_train, y_train, X_valid, y_valid)

# a = 'Master_Change_Learning_0304.csv'
# b = 'Master_Change_Test_0304.csv'
# test_main(a, b)
