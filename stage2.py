### Eddie Chen '16
### Stage 2, Finer Model (basic linear regression)
from sys import argv
import numpy as np
import pandas as pd
import math
import statsmodels.api as sm
from sklearn import linear_model
from sklearn.cross_validation import cross_val_predict
from sklearn import preprocessing
import matplotlib.pyplot as plt
import stage2plot
#print "============= Stage 2 Model, version 1.1 ===================="

# input dataset
print "Loading Data..."
# input dataset
if (len(argv) < 2):
    raise NameError('No filename specified')

# learning set
filename = argv[1]
# data = np.genfromtxt(filename, comments="#", delimiter=',', autostrip=True, skip_header=1)
# (height, width) = data.shape
dd = pd.read_csv(filename, index_col=0)
# print dd.dtypes
X = dd[dd.columns[3::]]
print "======== Simple Multi-Regression to Pick Variables =========="
# The p-value for each term tests the null hypothesis that the coefficient
# is equal to zero (no effect). A low p-value (< 0.05) indicates that you can
# reject the null hypothesis. In other words, a predictor that has a
# low p-value is likely to be a meaningful addition to your model because
# changes in the predictor's value are related to changes in the response variable.
print X.columns
### possible code for standardization, but no help really
# ts_stand = (dd - dd.mean()) / dd.std()
# X = ts_stand[ts_stand.columns[3::]]

y = dd['SP_Change']

print "Run a naive OLS multi-regression first..."
#selStats = raw_input("Run a multi-regression first? Y/N: ")
# if ((selStats == 'Y') or (selStats == 'y')):
X = sm.add_constant(X)
est = sm.OLS(y, X).fit()
print est.summary()
print "============================================================="
print "The following variables were selected for being more significant than a dummy var:"
# shortlist
X = dd[['Nikkei225Change','CrudeChange','YieldSpreadChange','NasdaqChange','BSE30Change','CACChange',
            'HSIChange','NYSEChange','Russell2000Change','SHChange']]
print X.columns
print "============================================================="
print "Loading Validation Data..."
testName = argv[2]
valid = pd.read_csv(testName, index_col=0)
X_valid = valid[['Nikkei225Change','CrudeChange','YieldSpreadChange','NasdaqChange','BSE30Change','CACChange',
            'HSIChange','NYSEChange','Russell2000Change','SHChange']]
y_valid = valid['SP_Change']

train = pd.read_csv(filename, index_col=0)
X_train = train[['Nikkei225Change','CrudeChange','YieldSpreadChange','NasdaqChange','BSE30Change','CACChange',
            'HSIChange','NYSEChange','Russell2000Change','SHChange']]
y_train = train['SP_Change']
#build a linear regression
# standardize y first
scaler = preprocessing.StandardScaler().fit(y_train)
y_train = scaler.transform(y_train)
y_valid = scaler.transform(y_valid)

lr = linear_model.LinearRegression(normalize = True)
print lr.fit(X_train, y_train)
y_valid_predict = lr.predict(X_valid)
print "Linear Model has a score of %.3f" % lr.score(X_valid, y_valid)
print "Linear Model has a RMSE of %.3f" % math.sqrt(np.mean((y_valid_predict - y_valid)**2))
#predicted = cross_val_predict(lr, boston.data, y, cv=10)
nlr = linear_model.LinearRegression(normalize = True)
predicted = cross_val_predict(nlr, X_valid, y_valid, cv=10)
print "Cross-validated Linear Model has a RMSE of %.3f" % math.sqrt(np.mean((predicted - y_valid)**2))
# fig, ax = plt.subplots()
# ax.scatter(y_valid, predicted)
# ax.plot([y_valid.min(), y_valid.max()], [y_valid.min(), y_valid.max()], 'k--', lw=4)
# ax.set_xlabel('Measured')
# ax.set_ylabel('Predicted')
# plt.show()
stage2plot.plot(y_valid_predict, y_valid)
stage2plot.plot(predicted, y_valid)
