# Eddie Chen '16, Neural Network to predict color, regression
# input two files, output predictions for S&P changes

# this is the regressor version (continuous value)
# TODO: automate column number

from sknn.mlp import Regressor, Layer
import numpy as np
from sys import argv
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler
from sklearn import preprocessing
from numpy import linalg as LA
import math

def neural(filename, testName, label, xstart):
    #reference: http://scikit-neuralnetwork.readthedocs.org/en/latest/module_mlp.html#regressor
    print "Neural Network Demo, version 1.0"
    # input dataset
    if (filename == ""):
        raise NameError('No filename specified')

    print "Loading Data..."

    # learning set
    # filename = argv[1]
    data = np.genfromtxt(filename, comments="#", delimiter=',', autostrip=True, skip_header=1)
    (height, width) = data.shape
    print "data dimension is "+str(data.shape)
    #
    # dayClassCol = 6
    # spCol = 5
    # spDailyChange = 8
    # upordown = 13
    # bucket = 11
    # shiller = 14
    # # toggle column number
    # label = spDailyChange

    y_train = data[0:,label:label+1] #target array
    X_train = data[0:,xstart:width] #input
    print "y_train is "
    print y_train
    ### test set
    # testName = argv[2]
    data = np.genfromtxt(testName, comments="#", delimiter=',', autostrip=True, skip_header=1)
    (height, width) = data.shape

    y_valid = data[0:,label:label+1] #dayClass, output array
    X_valid = data[0:,xstart:width] #input

    print "Setting up neural network..."

    #scale data # doesn't work yet
    # pipeline = Pipeline([
    #         ('min/max scaler', MinMaxScaler(feature_range=(0.0, 1.0))),
    #         ('neural network', Classifier(layers=[Layer("Linear")], n_iter=25))])
    # pipeline.fit(X_train, y_train)
    scaler = preprocessing.StandardScaler().fit(X_train)
    X_train = scaler.transform(X_train)
    # scaler = preprocessing.StandardScaler().fit(y_train)
    # y_train = scaler.transform(y_train)
    scaler = preprocessing.StandardScaler().fit(X_valid)
    X_valid = scaler.transform(X_valid)
    # scaler = preprocessing.StandardScaler().fit(y_valid)
    # y_valid = scaler.transform(y_valid)


    nn = Regressor(
        layers=[
            Layer("Rectifier", units=10000),
            Layer("Linear")],
        learning_rate=0.01,
        n_iter=100)

    print "Running neural network..."
    nn.fit(X_train, y_train)

    y_valid_predict = nn.predict(X_valid)

    print "Done, calculating errors..."

    sum = 0

    #calculate error, RMSE style
    for i in xrange(len(y_valid)):
        sum = sum + pow(abs(y_valid[i] - y_valid_predict[i]),2)

    avg = math.sqrt(sum / len(y_valid))

    print "Root Mean Square Error is: "
    print str(avg*100)+"%"

    # output new values
    outname = "predicted_neural_"+filename
    #print "Comparison: expected vs predicted"
    #outArray = np.concatenate((y_valid_predict,y_valid),axis=0)
    # print outname
    # print np.append(y_valid,y_valid_predict,axis = 1)
    # np.savetxt(outname, np.append(y_valid,y_valid_predict,axis = 1))
    # print "Output saved to "+outname
    return y_valid_predict;
