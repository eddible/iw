                            OLS Regression Results                            
==============================================================================
Dep. Variable:              SP_Change   R-squared:                       0.293
Model:                            OLS   Adj. R-squared:                  0.258
Method:                 Least Squares   F-statistic:                     8.325
Date:                Wed, 24 Feb 2016   Prob (F-statistic):           1.50e-19
Time:                        20:31:20   Log-Likelihood:                 1517.5
No. Observations:                 402   AIC:                            -2995.
Df Residuals:                     382   BIC:                            -2915.
Df Model:                          19                                         
Covariance Type:            nonrobust                                         
=====================================================================================
                        coef    std err          t      P>|t|      [95.0% Conf. Int.]
-------------------------------------------------------------------------------------
const                 0.0002      0.000      0.600      0.549        -0.000     0.001
TempChange           -0.0017      0.002     -0.956      0.340        -0.005     0.002
Nikkei225Change       0.0123      0.028      0.441      0.659        -0.042     0.067
DowJonesChange       -0.1663      0.085     -1.963      0.050        -0.333     0.000
CrudeChange          -0.0683      0.029     -2.338      0.020        -0.126    -0.011
RMBChange            -0.4149      0.299     -1.388      0.166        -1.003     0.173
USDChange            -0.0805      0.058     -1.394      0.164        -0.194     0.033
GoldChange            0.0455      0.027      1.680      0.094        -0.008     0.099
YenChange             0.0179      0.055      0.326      0.745        -0.090     0.126
YieldSpreadChange     0.0076      0.008      0.943      0.346        -0.008     0.023
NasdaqChange         -0.2629      0.070     -3.760      0.000        -0.400    -0.125
BSE30Change           0.0481      0.026      1.849      0.065        -0.003     0.099
CACChange            -0.0174      0.051     -0.341      0.733        -0.118     0.083
DAXChange             0.0393      0.055      0.710      0.478        -0.070     0.148
FTSEChange            0.0555      0.042      1.329      0.185        -0.027     0.138
TBillChange           0.0013      0.001      1.663      0.097        -0.000     0.003
HSIChange             0.1142      0.035      3.238      0.001         0.045     0.184
NYSEChange            0.1435      0.056      2.574      0.010         0.034     0.253
Russell2000Change     0.2645      0.045      5.889      0.000         0.176     0.353
SHChange             -0.0190      0.020     -0.946      0.345        -0.059     0.021
==============================================================================
Omnibus:                       19.116   Durbin-Watson:                   2.118
Prob(Omnibus):                  0.000   Jarque-Bera (JB):               50.433
Skew:                          -0.073   Prob(JB):                     1.12e-11
Kurtosis:                       4.729   Cond. No.                     1.06e+03
==============================================================================

Warnings:
[1] Standard Errors assume that the covariance matrix of the errors is correctly specified.
[2] The condition number is large, 1.06e+03. This might indicate that there are
strong multicollinearity or other numerical problems.
