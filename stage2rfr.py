# Stage II, Random Forest Regression + ExtraTreesRegressor + AdaBoostRegressor
# ensemble techniques (combining decision trees with bagging)
# combined with Gaussain process too

# Eddie Chen '16, March 13, 2016
# References: http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html#sklearn.ensemble.RandomForestRegressor
# http://math.bu.edu/people/mkon/MA751/L18RandomForests.pdf

import time
import math
import numpy as np
from sys import argv
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor, AdaBoostRegressor
from sklearn.pipeline import Pipeline
from sklearn import preprocessing
from sklearn.cross_validation import cross_val_score
from sklearn.gaussian_process import GaussianProcess
import stage2plot


def rfr(X_train, y_train, X_valid, y_valid):
    print "training X dataset dimension is", X_train.shape
    print "=============== Running Random Forest Regression ==============="
    # Create a regressor:
    estimator = RandomForestRegressor(n_jobs = -1)
    #score = cross_val_score(estimator, X_valid, y_valid).mean()
    #print("Mean Cross Validation Score = %.2f" % score)
    estimator.fit(X_train, y_train)
    predicted = estimator.predict(X_valid)
    #stage2plot.plot(predicted, y_valid)
    print("Random Forest Regression has RMSE of %.3f" % math.sqrt(np.mean((predicted - y_valid) ** 2)))
    print(" has score of %.3f" % estimator.score(X_valid, y_valid))
    return predicted
    #res = np.column_stack((res,scalerY.inverse_transform(predicted)))


def extratree(X_train, y_train, X_valid, y_valid):
    # ExtraTreesRegressor
    print "=============== Running Extra Tree Regression ==============="
    et = ExtraTreesRegressor(n_jobs = -1) #use as many cores as possible
    #score = cross_val_score(et, X_valid, y_valid).mean()
    #print("Mean Cross Validation Score = %.2f" % score)
    et.fit(X_train, y_train)
    predicted = et.predict(X_valid)
    #stage2plot.plot(predicted, y_valid)
    print("Extra Tree Regression has RMSE of %.3f" % math.sqrt(np.mean((predicted - y_valid) ** 2)))
    print(" has score of %.3f" % et.score(X_valid, y_valid))
    #res = np.column_stack((res,scalerY.inverse_transform(predicted)))
    return predicted

def ada(X_train, y_train, X_valid, y_valid):
    # AdaBoostRegressor
    print "=============== Running Ada Boost Regression ==============="
    ada = AdaBoostRegressor()
    #score = cross_val_score(ada, X_valid, y_valid).mean()
    #print("Mean Cross Validation Score = %.2f" % score)
    ada.fit(X_train, y_train)
    predicted = ada.predict(X_valid)
    #stage2plot.plot(predicted, y_valid)
    print("Ada Boost Regression has RMSE of %.3f" % math.sqrt(np.mean((predicted - y_valid) ** 2)))
    print(" has score of %.3f" % ada.score(X_valid, y_valid))
    #res = np.column_stack((res,scalerY.inverse_transform(predicted)))
    return predicted
# Gaussian Process Regression
def gaussian(X_train, y_train, X_valid, y_valid):
    print "=============== Running Gaussian Process Regression ==============="
    # reference: http://scikit-learn.org/stable/auto_examples/gaussian_process/plot_gp_regression.html
    gp = GaussianProcess(theta0=0.1, thetaL=.001, thetaU=1.)
    gp.fit(X_train, y_train)
    y_pred, MSE = gp.predict(X_valid, eval_MSE=True)
    sigma = np.sqrt(MSE)
    print "Gaussian Process MSE is", math.sqrt(np.mean((y_pred - y_valid) ** 2))
    return y_pred

# Restricted Boltzmann Machine
# this is for binary classifier, tells us activation layer, not applicable yet
# def rbm(X_train, X_valid):
#     #reference: http://deeplearning4j.org/restrictedboltzmannmachine.html
#     # used code from https://github.com/echen/restricted-boltzmann-machines/blob/master/rbm.py
#     (height, width) = X_train.shape
#     r = echen_rbm.RBM(num_visible = width, num_hidden = 1)
#     r.train(X_train, max_epochs = 5000)
#     print(r.weights)
#     y_predict = r.run_visible(X_valid)

def test_main():
    """
        Main, TODO: automate parameter input
    """
    # input dataset
    print "Loading Data..."
    # input dataset
    if (len(argv) < 2):
        raise NameError('No filename/not enough filenames specified')

    filename = argv[1]
    data = np.genfromtxt(filename, comments="#", delimiter=',', autostrip=True, skip_header=1)
    (height, width) = data.shape
    print "data dimension is "+str(data.shape)
    # dd = pd.read_csv(filename)
    # print "for your reference, columns' name and index are as follows: "
    # for iter in range(len(dd.columns)):
    #     print(dd.columns[iter],iter)
    # toggle column number
    # label = int(raw_input('y variable column: '))
    # xstart = int(raw_input('X variable column start: '))
    #
    # print "Selected y as "+str(label)
    # print "Selected x starts at "+str(xstart)
    label = 3
    xstart = 4
    # test set
    testName = argv[2]
    valid = np.genfromtxt(testName, comments="#", delimiter=',', autostrip=True, skip_header=1)
    #
    # print('Learning set is ', filename)
    #
    # print('Validation set is ', testName)
    size = width - xstart

    # standardize data
    data = preprocessing.Imputer().fit_transform(data)
    y_train_original = data[0:,label:label+1]
    valid = preprocessing.Imputer().fit_transform(valid)
    y_valid_original = valid[0:,label:label+1]

    scaler = preprocessing.StandardScaler().fit(data)
    scalerY = preprocessing.StandardScaler().fit(y_train_original)

    data = scaler.transform(data)
    valid = scaler.transform(valid)
    # extract X and y
    # y_train = data[0:,label:label+1]
    y_train = scalerY.transform(y_train_original)
    y_train = y_train.ravel()
    X_train = data[0:,xstart:width] #input

    # y_valid = valid[0:,label:label+1]
    y_valid = scalerY.transform(y_valid_original)
    y_valid = y_valid.ravel()
    X_valid = valid[0:,xstart:width] #input
    res = y_valid_original


    # Run Gaussian Process Regression
    gp = gaussian(X_train, y_train, X_valid, y_valid)
    # plot it
    stage2plot.plot(scalerY.inverse_transform(gp), scalerY.inverse_transform(y_valid))
    res = np.column_stack((res,scalerY.inverse_transform(gp)))

    #print res
    np.savetxt('test.csv', res, delimiter=',')
