# pre-process data
import numpy as np
import math
from sklearn.cross_validation import train_test_split
from sklearn.preprocessing import Imputer
import bucketing
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn import preprocessing
import bucketing
from sklearn import neighbors
from sklearn import cluster
import datetime
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.svm import SVC
from time import time
from operator import itemgetter
from scipy.stats import randint as sp_randint
import GA_stage1

def compare_bench(y, bench):
    sum = 0
    for i in xrange(len(y)):
        if (y[i] != bench[i]):
            sum = sum+1
    print("Hit ratio is %.3f" % (1.0 - sum/(len(y) * 1.0)))
    print("    RMSE is %.3f" % math.sqrt(np.mean((y - bench) ** 2)))
    return sum

def confusion_matrix_print(y, bench):
    cm = confusion_matrix(bench, y)
    np.set_printoptions(precision=2)
    print('Confusion matrix, without normalization')
    print(cm)

def plot_confusion_matrix(cm, target_names, title='Confusion matrix', cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(target_names))
    plt.xticks(tick_marks, target_names, rotation=45)
    plt.yticks(tick_marks, target_names)
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

# Utility function to report best scores
def report(grid_scores, n_top=3):
    top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        print("Model with rank: {0}".format(i + 1))
        print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
              score.mean_validation_score,
              np.std(score.cv_validation_scores)))
        print("Parameters: {0}".format(score.parameters))
        print("")

totalstart = time()

imp = Imputer(missing_values='NaN', strategy='mean', axis=0)

filename = 'stage1datastream_input_all.csv'

data = np.genfromtxt(filename, comments="#", delimiter=',', autostrip=True, skip_header=1)
(height, width) = data.shape
label = 1
xstart = 2

print "data dimension is "+str(data.shape)

y = (data[1:, 1:2] - data[:-1, 1:2])/data[:-1, 1:2]
y = y[1:]
y = bucketing.bucketing(y, np.mean(y),np.std(y))
print np.unique(y)

X = data[:,xstart:]
X = (X[1:, :]-X[:-1,:])/X[:-1,:]
print X.shape
X[np.isnan(X)] = 0
X[np.isfinite(X) == False]=0
print np.any(np.isnan(X))
print np.all(np.isfinite(X))

# imp.fit(X)
## X = Imputer().fit_transform(X)
# X = imp.transform(X)
X = X[:-1,:]
print X.shape


imp = Imputer(missing_values='NaN', strategy='most_frequent', axis=0)
imp.fit(y)
y = imp.transform(y)
# y[np.isnan(y)] = 0
print y.shape

X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=0.28, random_state=42)
# X_train = X[0:5500, :]
# X_valid = X[5500:7666, :]
# y_train = y[0:5500]
# y_valid = y[5500:7666]
# print np.isnan(X)
# print np.isnan(y)

scaler = preprocessing.StandardScaler().fit(X_train)
X_train = scaler.transform(X_train)
X_valid = scaler.transform(X_valid)

# print np.isnan(X_train)
# print np.isnan(y_train)
print "--- Starting Genetic Algortihm ---"
GA_stage1.GA(X_train, y_train, X_valid, y_valid)

print "------Beginning SVM Run------"
#
#
print "For benchmarking, Setting up k-nearest neighbor classifier..."
knn = neighbors.KNeighborsClassifier()
knn.fit(X_train, y_train.ravel())
y_valid_predict = knn.predict(X_valid)
#res = np.column_stack((res,y_valid_predict))
res = y_valid_predict;
#check accuracy
print "Done, checking KNN accuracy..."
sum = 0
for i in xrange(len(y_valid)):
    if (y_valid[i] != y_valid_predict[i]):
        sum = sum + 1

print "Hit ratio: "+str(1- sum * 1.0 /len(y_valid_predict))
print("    score is %.3f" % knn.score(X_valid, y_valid))
print("    RMSE is %.3f" % math.sqrt(np.mean((y_valid_predict - y_valid) ** 2)))
print ""
### this version is much faster
print "Setting up linear SVM..."
svc = svm.LinearSVC()
svc.fit(X_train, y_train.ravel())
y_valid_predict = svc.predict(X_valid)
res = np.column_stack((res,y_valid_predict))
#np.append(res,y_valid_predict,axis = 1)
print "Done, checking linear SVM accuracy..."
sum = 0
for i in xrange(len(y_valid)):
    if (y_valid[i] != y_valid_predict[i]):
        sum = sum + 1

print "Hit ratio: "+str(1- sum * 1.0 /len(y_valid_predict))
print("    score is %.3f" % svc.score(X_valid, y_valid))
print("    RMSE is %.3f" % math.sqrt(np.mean((y_valid_predict - y_valid) ** 2)))
print ""

# print "Setting up poly-kernel SVM..."
# svc = svm.SVC(kernel='poly')
# svc.fit(X_train, y_train.ravel())
# y_valid_predict = svc.predict(X_valid)
# res = np.column_stack((res,y_valid_predict))
# print "Done, checking Ploy-kernel SVM accuracy..."
# sum = 0
# for i in xrange(len(y_valid)):
#     if (y_valid[i] != y_valid_predict[i]):
#         sum = sum + 1
#
# print "Hit ratio: "+str(1.0-sum * 1.0/len(y_valid_predict))
# print("    score is %.3f" % svc.score(X_valid, y_valid))
# print("    RMSE is %.3f" % math.sqrt(np.mean((y_valid_predict - y_valid) ** 2)))
# print ""

print "Setting up RBF-kernel SVM, using parameters from Grid Search..."
svc = svm.SVC(kernel='rbf', C=1, gamma=0.001)
svc.fit(X_train, y_train.ravel())
y_valid_predict = svc.predict(X_valid)
res = np.column_stack((res,y_valid_predict))
print "Parameters for this predictor are:"
print svc.get_params()
#np.append(res,y_valid_predict,axis = 1)
print "Done, checking RBF-kernel SVM accuracy..."
sum = 0
for i in xrange(len(y_valid)):
    if (y_valid[i] != y_valid_predict[i]):
        sum = sum + 1

print "Hit ratio: "+str(1- sum * 1.0/len(y_valid_predict))
print("    score is %.3f" % svc.score(X_valid, y_valid))
print("    RMSE is %.3f" % math.sqrt(np.mean((y_valid_predict - y_valid) ** 2)))
print ""
#
#
# # run grid search for best parameter settings
# y_train = y_train.ravel()
# print "Running Grid Search for SVM Parameters (C, kernel, gamma)"
# print "Testing rbf /linear /polynomial /sigmoid kernels"
# # tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
# #                  'C': [1, 10, 100, 1000]},
# #                 {'kernel': ['linear'], 'C': [1, 10, 100, 1000]},
# #                 {'kernel': ['poly'], 'C': [1, 10, 100, 1000]},
# #                 {'kernel': ['sigmoid'], 'C': [1, 10, 100, 1000]}
# #                 ]
# tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
#                  'C': [1, 10, 100, 1000]},
#                  {'kernel': ['linear'], 'C': [1, 10]},
#                  {'kernel': ['poly']},
#                  {'kernel': ['sigmoid'], 'C':[1,10]}] # for testing purpose
# svc = svm.SVC()
# print "......Searching......"
# start = time()
# clf = GridSearchCV(svc, tuned_parameters, n_jobs=-1, cv=3)
# clf.fit(X_train, y_train)
# print ""
# print("GridSearchCV took %.2f seconds for %d candidate parameter settings."
#   % (time() - start, len(clf.grid_scores_)))
# report(clf.grid_scores_)
# print("Best parameters set found on the training set:")
# print(clf.best_params_)
#
# y_valid_predict = clf.predict(X_valid)
# print "/------------------------------/"
# print "Done, checking SVM accuracy..."
# sum = 0
# for i in xrange(len(y_valid)):
#     if (y_valid[i] != y_valid_predict[i]):
#         sum = sum + 1
# print "Hit ratio: "+str(1 - sum * 1.0/len(y_valid_predict))
# print("    score is %.3f" % clf.score(X_valid, y_valid))
# print("    RMSE is %.3f" % math.sqrt(np.mean((y_valid_predict - y_valid) ** 2)))
# res = np.column_stack((res,y_valid_predict))
#
# print ""
# print "Total time taken: %.2f seconds" % (time() - totalstart)
# print ""
# print "------End of SVM Run------"
print "------End of SVM Run------"


print ""
y_bag = bucketing.bagging(res)
compare_bench(y_bag, y_valid)
confusion_matrix_print(y_bag, y_valid)

cm = confusion_matrix(y_valid, y_bag)
plt.figure()
target_names = np.unique(y_bag)
plot_confusion_matrix(cm, target_names)
plt.show()
