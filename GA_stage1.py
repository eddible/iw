# Stage I, genetic algorithm
# Eddie Chen '16, April 11, 2016

# references: http://lethain.com/genetic-algorithms-cool-name-damn-simple/
# Choudhry, Rohit, and Kumkum Garg. "A hybrid machine learning system for stock
# market forecasting." World Academy of Science, Engineering and Technology
# 39.3 (2008): 315-318.

# A Genetic Algorithm is now used to select a set of salient features from among them.
# The selected features are used as inputs to a Support Vector Machine. The purpose
# here is to obtain an optimal subset of features which produce the best possible results.

# use a binary array to code if a feature is selected a not

""" You can select different functions for fitness, choices include:
    SVR (Linear, Poly or RBF Kernel), Linear Regression (R^2)

    Parameters for evolution:
    retain=0.2, random_select=0.05, mutate=0.1

    Population count: 100 (this directly impacts the running speed)
    Generation count: 10 (this directly impacts the running speed)
"""
from random import randint, random
from sys import argv
import numpy as np
import pandas as pd
from sklearn import svm, preprocessing
from operator import add
import math
from sklearn.cross_validation import cross_val_predict
from sklearn import linear_model
from progress.bar import Bar

def individual(size):
    'Create a member of the population.' # create individual chromosome
    return [ randint(0,1) for x in xrange(size) ]

def population(count, size):
    """
    Create a number of individuals (i.e. a population).
    """
    return [ individual(size) for x in xrange(count) ]

def fitness(individual, X_train, y_train, X_valid, y_valid):
    """
    Determine the fitness of an individual. smaller is better.
    individual: the individual to evaluate
    target: the target number individuals are aiming for
    """
    err = svc_label(individual, X_train, y_train, X_valid, y_valid) #run SVR to determine accuracy
    #err = linearReg(individual, X_train, y_train, X_valid, y_valid)
    return err #smaller, better

def grade(pop, X_train, y_train, X_valid, y_valid):
    'Find average fitness for a population.'
    # summed = reduce(add, (fitness(x, X_train, y_train, X_valid, y_valid, label, xstart) for x in pop))
    # return summed / (len(pop) * 1.0)
    summed = 0;
    for x in pop:
        summed = summed + fitness(x, X_train, y_train, X_valid, y_valid)
    #return summed / (len(pop) * 1.0)
    return summed

def evolve(pop, X_train, y_train, X_valid, y_valid, retain=0.2, random_select=0.05, mutate=0.1):
    graded = [ (fitness(x, X_train, y_train, X_valid, y_valid), x) for x in pop]
    graded = [ x[1] for x in sorted(graded, reverse=True)]
    ### Temporarily print top child
    # print "Top child is ", graded[0]

    retain_length = int(len(graded)*retain)
    parents = graded[:retain_length]
    # randomly add other individuals to
    # promote genetic diversity
    for individual in graded[retain_length:]:
        if random_select > random():
            parents.append(individual)
    #print "after random select, parents#: ", len(parents)
    #print "parents are", parents
    # mutate some individuals
    for individual in parents:
        if mutate > random():
            pos_to_mutate = randint(0, len(individual)-1)
            individual[pos_to_mutate] = randint(
                0, 1)
    #print "after mutation..."
    # crossover parents to create children
    parents_length = len(parents)
    desired_length = len(pop) - parents_length
    children = []
    #print "desired_length is ", desired_length
    while len(children) < desired_length:
        male = randint(0, parents_length-1)
        female = randint(0, parents_length-1)
        if male != female:
            male = parents[male]
            female = parents[female]

            half = len(male) / 2 # use half of male and half of female;
            child = male[:half] + female[half:]
            #TODO: consider dominant versus recessive? figure out how it works
            # child = male;
            # for i in xrange(len(male)):
            #     if ((male[i] + female[i]) >= 1):
            #         child[i] = 1
            #     else:
            #         child[i] = 0
            children.append(child)
    parents.extend(children)
    return parents

def svc_label(individual, X_train, y_train, X_valid, y_valid):
    """
    svr to calculate the error rate; data and valid are both ndarrays
    """
    print "Running SVR for",
    print individual
    sel = individual;
    #print("individual is ", sel);
    (h, width) = X_train.shape
    all_data = X_train
    newdata = y_train
    for x in xrange(len(sel)):
        if (sel[x] == 1):
            newdata = np.hstack((newdata, all_data[:,x:x+1]))
    #print newdata.shape
    (dim, wid) = newdata.shape
    if (wid==1):
        return -10000 #to penalize empty chromosome
    # y_train = data[0:,label:label+1]
    # X_train = data[0:,xstart:width] #input
    y_train_new = newdata[0:, 0:1]
    X_train_new = newdata[0:,1:wid]
    # do the same for Validation
    all_data = X_valid
    newdata = y_valid
    for x in xrange(len(sel)):
        if (sel[x] == 1):
            newdata = np.hstack((newdata, all_data[0:,x:x+1]))

    (dim, wid) = newdata.shape
    y_valid_new = newdata[0:, 0:1]
    X_valid_new = newdata[0:,1:wid]

    svc = svm.SVC(kernel='rbf', C=1, gamma=0.001)

    svc.fit(X_train_new, y_train_new.ravel())
    y_valid_predict = svc.predict(X_valid_new)
    score = svc.score(X_valid_new, y_valid_new.ravel())
    hit = 0
    for i in xrange(len(y_valid_new)):
        if (y_valid_predict[i] == y_valid[i]):
            hit = hit+1
    hit = hit/len(y_valid_new)
    #return math.sqrt(sum) ##bigger the worse
    return score ##the bigger the better, related to how I sort fitness

def print_label(individual, filename, xstart):
    dd = pd.read_csv(filename)
    print "Selected columns' name and index are as follows: "
    for iter in range(len(dd.columns) - xstart):
        if (individual[iter] == 1):
            print(dd.columns[iter + xstart],iter+xstart)

def GA(X_train, y_train, X_valid, y_valid):
    """
        Main, TODO: automate parameter input
    """
    print "Running GA..."
    (height, size) = X_train.shape
    print X_train.shape

    population_count = 20 #TODO: get this value from input
    generations = 10 #number of generations to run
    print("Building initial population with length %d..." % size)
    p = population(population_count, size)
    print "Top child is ", p[0]
    fitness_history = [grade(p, X_train, y_train, X_valid, y_valid),]
    #
    # bar = Bar('Processing', max=generations, suffix='%(percent)d%%') #progress bar to print progress

    for i in xrange(generations):
        print("...Generation %d" % i)
        p = evolve(p, X_train, y_train, X_valid, y_valid)
        print "top child is, ", p[0]
        fitness_history.append(grade(p, X_train, y_train, X_valid, y_valid))

    # bar.finish()
    # # for datum in fitness_history:
    # #    print datum
    filename = 'stage1datastream_input_all.csv'
    print_label(p[0], filename, 2)

    """
        print out the SVM prediction
    """
    print "Piping into the SVM..."
    sel = p[0];
    #print("individual is ", sel);

    all_data = X_train
    newdata = y_train
    for x in xrange(len(sel)):
        if (sel[x] == 1):
            newdata = np.hstack((newdata, all_data[0:,x:x+1]))
    print newdata.shape
    (dim, wid) = newdata.shape
    X_train_new = newdata[0:,1:wid]
    # do the same for Validation
    all_data = X_valid
    newdata = y_valid
    for x in xrange(len(sel)):
        if (sel[x] == 1):
            newdata = np.hstack((newdata, all_data[0:,x:x+1]))
    print newdata.shape
    (dim, wid) = newdata.shape
    X_valid_new = newdata[0:,1:wid]

    svc = svm.SVC(kernel='rbf', C=1, gamma=0.001)
    #print X_train.shape
    svc.fit(X_train_new, y_train.ravel())
    y_valid_predict = svc.predict(X_valid_new)

    # The mean square error
    print("Root Mean Square Error: %.2f"% math.sqrt(np.mean((y_valid_predict - y_valid) ** 2)))
    # Explained variance score: 1 is perfect prediction
    print "Explained variance score: 1 is perfect prediction"
    print('Variance score: %.2f' % svc.score(X_valid_new, y_valid))
    hit = 0
    for i in xrange(len(y_valid)):
        if (y_valid_predict[i] == y_valid[i]):
            hit = hit+1
    hit = hit/len(y_valid)
    print("Hit ratio is %.3f" % hit)
    #print "error term is ", np.mean((y_valid_predict - y_valid) ** 2)
    return y_valid_predict
