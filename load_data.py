### prep data for stage 1
### April 6, 2016
### Eddie Chen '16
import pandas_datareader.data as web
import datetime
import numpy as np
import bucketing
from sklearn.preprocessing import Imputer

imp = Imputer(missing_values='NaN', strategy='most_frequent', axis=0)

start = datetime.datetime(1987, 1, 2)
end = datetime.datetime(2016, 1, 1)


'''
    load y label for stage 1, which is end-day sp change
'''
sp = web.DataReader("^GSPC", 'yahoo', start, end)
print type(sp)
# # preprocess data #TODO: use imputer
# spOpen[spOpen == np.nan] = 0.000000001
# y = np.diff(spOpen) / spOpen[:-1] * 100.
# #print(y) #y is the change next day to today
#
# y = bucketing.bucketing(y,np.mean(y), np.std(y))
#
# print spOpen.shape
# print y.shape
#


'''
    load x data
'''
'''
    load related indices
'''
nk = web.DataReader("^N225", 'yahoo', start, end)
nk = nk['Open']
sh = web.DataReader("^SSEC", 'yahoo', start, end)
sh = sh['Open']
hk = web.DataReader("^HSI", 'yahoo', start, end)
hk = hk['Open']
india = web.DataReader("^BSESN", 'yahoo', start, end)
india = india['Open']
asia_index =sp.join(nk, how='outer', rsuffix='_Nikkei').join(sh, how='outer', rsuffix='_Shanghai').join(hk, how='outer', rsuffix='_HK').join(india, how='outer', rsuffix='_BSE')

paris = web.DataReader("^FCHI", 'yahoo', start, end)
paris = paris['Open']
dax = web.DataReader("^GDAXI", 'yahoo', start, end)
dax = dax['Open']
ftse = web.DataReader("^FTSE", 'yahoo', start, end)
ftse = ftse['Open']

indices = asia_index.join(paris, how='outer', rsuffix='_CAC').join(dax, how='outer', rsuffix='_DAX').join(ftse, how='outer', rsuffix='_FTSE')


'''
    load macro economics data
'''

'''
    load commodities
'''
gold = web.DataReader("GOLDPMGBD228NLBM", 'fred', start, end)
oil = web.DataReader("MCOILWTICO", 'fred', start, end)

res = indices.join(gold, how='outer', rsuffix='_gold').join(oil, how='outer', rsuffix='_oil')
'''
    saving res
'''
res.to_csv('new_input_0406.csv')
