# Eddie Chen '16
# Feb 9, 2016, 20:57
# this program loads one specific metric and save it to csv

import pandas as pd
import pandas_datareader.data as web
import datetime

start = datetime.datetime(2006, 2, 1)
end = datetime.datetime(2016, 2, 15)

sp500 = web.DataReader("INDEXSP:.INX", 'google', start, end) #import SP500 data
sp500.to_csv('sp500_google.csv')
print sp500.head()
#
# temp=web.DataReader("^DJI", 'yahoo', start, end)
# temp.to_csv('DJIA_Fred.csv')
#
# df = pd.read_csv('DJIA_Fred.csv', index_col='Date', parse_dates=True)
# dji_open = df['Open']
# dji_close = df['Close']
# dji_change = dji_close - dji_open
#
# print(dji_change.head())

# rp = pd.read_csv('risk_premium.csv', index_col='Date')
# print(rp.head())

# temp=web.DataReader("NASDAQCOM", 'fred', start, end)
# temp.to_csv('Nasdaq_Fred.csv')
# nas = pd.read_csv('Nasdaq_Fred.csv', index_col='DATE')
# print nas.head()

# temp=web.DataReader("^N225", 'yahoo', start, end)
# temp.to_csv('Nikkei225_yahoo.csv')
#
# df = pd.read_csv('Nikkei225_yahoo.csv', index_col='Date', parse_dates=True)
# nk_open = df['Open']
# nk_close = df['Close']
# nk_change = nk_close - nk_open
# print nk_change.head()

# temp=web.DataReader("RU2000PR", 'fred', start, end)
# temp.to_csv('R2000_Fred.csv')
# r2000 = pd.read_csv('R2000_Fred.csv', index_col='DATE')
# print r2000.head()

# data = pd.read_csv('Macro_Crude_FRED-DCOILWTICO.csv', index_col='DATE')
# crude = data[::-1]
# data = pd.read_csv('Macro_RMBtoOneDollar.csv', index_col='DATE')
# RMB = data[::-1] #reverse it for bookkeeping
# print RMB.head()

# data = pd.read_csv('Macro_USDtoEURO.csv', index_col='DATE')
# usd2euro = data[::-1] #reverse it for bookkeeping

# data = pd.read_csv('Macro_WGC-GOLD_DAILY_USD.csv', index_col='Date')
# gold = data[::-1] #reverse it for bookkeeping

# data = pd.read_csv('Macro_Yen_to_One_Dollar.csv', index_col='DATE')
# Yen = data[::-1] #reverse it for bookkeeping
# print Yen.head()

# data = pd.read_csv('Micro_AAII_SENTIMENT.csv', index_col='Date')
# data = data[::-1] #reverse it for bookkeeping
# bullish_senti = data['Bullish']
# print bullish_senti.head()

# data = pd.read_csv('Micro_yield_spread.csv', index_col='DATE')
# yieldspread = data[::-1] #reverse it for bookkeeping

# df = pd.read_csv('Micro_BSE30.csv', index_col='Date', parse_dates=True)
# bse_open = df['Open']
# bse_close = df['Close']
# bse_change = bse_close - bse_open

# df = pd.read_csv('Micro_CAC40.csv', index_col='Date', parse_dates=True)
# df=df[::-1]
# cac_open = df['Open']
# cac_close = df['Close']
# cac_change = cac_close - cac_open

# df = pd.read_csv('Micro_DAX.csv', index_col='Date', parse_dates=True)
# df=df[::-1]
# dax_open = df['Open']
# dax_close = df['Close']
# dax_change = dax_close - dax_open

# df = pd.read_csv('Micro_FTSE1000.csv', index_col='Date', parse_dates=True)
# df=df[::-1]
# ftse_open = df['Open']
# ftse_close = df['Close']
# ftse_change = ftse_close - ftse_open

# df = pd.read_csv('Micro_HSI.csv', index_col='Date', parse_dates=True)
# df=df[::-1]
# hsi_open = df['Open']
# hsi_close = df['Close']
# hsi_change = hsi_close - hsi_open

# df = pd.read_csv('Micro_SHSECOMP.csv', index_col='Date', parse_dates=True)
# df=df[::-1]
# sh_open = df['Open']
# sh_close = df['Close']
# print sh_change #has same open and clse???

# data = pd.read_csv('Micro_FRED_3mo_TBill.csv', index_col='DATE')
# riskfree = data[::-1] #reverse it for bookkeeping

# data = pd.read_csv('Micro_USTREASURY-YIELD.csv', index_col='Date')
# teasure_yield = data[::-1] #reverse it for bookkeeping
# tenyr = teasure_yield['10 YR']
# print tenyr.head()

# sp_change
# cpi
# exp_infl
# indpro
# realGDP
# dji_change
# rp
# nas
# nk_change
# r2000
# crude
# RMB
# usd2euro
# gold
# Yen
# bullish_senti
# yieldspread
# bse_change
# cac_change
# dax_change
# ftse_change
# hsi_change
# nyse_change
# sh_close
# riskfree
# tenyr

# shiller = pd.read_csv('Shiller_CAPE.csv', index_col='Date', parse_dates=True)
# pe10 = shiller['P/E 10 CAPE']
# print pe10.head()