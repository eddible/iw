# Eddie Chen '16
# Feb 9, 2016, 20:57
# this program loads all the financial data and cleans it up

# Import all libraries needed for the part
import pandas as pd
import pandas_datareader.data as web
import datetime

def IsDF(var):
    if isinstance(var, pd.DataFrame):
        print "is a DataFrame"

start = datetime.datetime(2006, 2, 1)
end = datetime.datetime(2016, 2, 4)

### Import S&P 500
sp500 = web.DataReader("^GSPC", 'yahoo', start, end) #import SP500 data
sp500.to_csv('sp500_yahoo.csv')
df = pd.read_csv('sp500_yahoo.csv', index_col='Date', parse_dates=True)
sp_open = df['Open']
sp_close = df['Close']
# the problem here is open = yesterday's close;
# but we wanted to know what impact the info differential has
sp_change = sp_close - sp_open

# import more data
### Import CPI
# CPI = web.DataReader("CPIAUCSL", 'fred', start, end)
# CPI.to_csv('CPI_Fred.csv')
cpi = pd.read_csv('CPI_Fred.csv', index_col='DATE')

### Import Expected Inflation
# temp=web.DataReader("MICH", 'fred', start, end)
# temp.to_csv('MICH_Fred.csv')
# print(temp.head())
exp_infl = pd.read_csv('MICH_Fred.csv', index_col='DATE')

### Import industrial production INDPRO
# temp=web.DataReader("INDPRO", 'fred', start, end)
# temp.to_csv('INDPRO_Fred.csv')
# print(temp.head())
indpro = pd.read_csv('INDPRO_Fred.csv', index_col='DATE')

### Import real GDP (annual)
realGDP = pd.read_csv('realGDP_Fred.csv', index_col='DATE')

### Import Down Jones Avg
# temp=web.DataReader("^DJI", 'yahoo', start, end)
# temp.to_csv('DJIA_Fred.csv')
df = pd.read_csv('DJIA_Fred.csv', index_col='Date', parse_dates=True)
dji_open = df['Open']
dji_close = df['Close']
dji_change = dji_close - dji_open

### Import Risk Premium
rp = pd.read_csv('risk_premium.csv', index_col='Date')

### Import Nasdaq Composite
nas = pd.read_csv('Nasdaq_Fred.csv', index_col='DATE')

### Import Nikkei 225
df = pd.read_csv('Nikkei225_yahoo.csv', index_col='Date', parse_dates=True)
nk_open = df['Open']
nk_close = df['Close']
nk_change = nk_close - nk_open

### Import Russell 2000
r2000 = pd.read_csv('R2000_Fred.csv', index_col='DATE')

### Load Crude oil price
data = pd.read_csv('Macro_Crude_FRED-DCOILWTICO.csv', index_col='DATE')
crude = data[::-1] #reverse it for bookkeeping

### Import RMB to USD (for one Dollar)
data = pd.read_csv('Macro_RMBtoOneDollar.csv', index_col='DATE')
RMB = data[::-1] #reverse it for bookkeeping

### Import USD to EURO (for one Eurp)
data = pd.read_csv('Macro_USDtoEURO.csv', index_col='DATE')
usd2euro = data[::-1] #reverse it for bookkeeping

### Import Gold price in USD
data = pd.read_csv('Macro_WGC-GOLD_DAILY_USD.csv', index_col='Date')
gold = data[::-1] #reverse it for bookkeeping

### Import YEN to USD (for one Dollar)
data = pd.read_csv('Macro_Yen_to_One_Dollar.csv', index_col='DATE')
Yen = data[::-1] #reverse it for bookkeeping

### Import Market Sentiment
data = pd.read_csv('Micro_AAII_SENTIMENT.csv', index_col='Date')
data = data[::-1] #reverse it for bookkeeping
bullish_senti = data['Bullish'] # bullish investor percentage

### Import Yield Spread
data = pd.read_csv('Micro_yield_spread.csv', index_col='DATE')
yieldspread = data[::-1] #reverse it for bookkeeping

### Import BSE30 (india)
df = pd.read_csv('Micro_BSE30.csv', index_col='Date', parse_dates=True)
df=df[::-1]
bse_open = df['Open']
bse_close = df['Close']
bse_change = bse_close - bse_open

### Import CAC40
df = pd.read_csv('Micro_CAC40.csv', index_col='Date', parse_dates=True)
df=df[::-1]
cac_open = df['Open']
cac_close = df['Close']
cac_change = cac_close - cac_open

### Import DAX
df = pd.read_csv('Micro_DAX.csv', index_col='Date', parse_dates=True)
df=df[::-1]
dax_open = df['Open']
dax_close = df['Close']
dax_change = dax_close - dax_open

### Import FTSE1000
df = pd.read_csv('Micro_FTSE1000.csv', index_col='Date', parse_dates=True)
df=df[::-1]
ftse_open = df['Open']
ftse_close = df['Close']
ftse_change = ftse_close - ftse_open

### Import Hong Kong Heng Seng Index
df = pd.read_csv('Micro_HSI.csv', index_col='Date', parse_dates=True)
df=df[::-1]
hsi_open = df['Open']
hsi_close = df['Close']
hsi_change = hsi_close - hsi_open

### Import NYSE
df = pd.read_csv('Micro_NYSE.csv', index_col='Date', parse_dates=True)
df=df[::-1]
nyse_open = df['Open']
nyse_close = df['Close']
nyse_change = nyse_close - nyse_open

### Import Shanghai Stock Exchange Composite
df = pd.read_csv('Micro_SHSECOMP.csv', index_col='Date', parse_dates=True)
df=df[::-1]
sh_open = df['Open']
sh_close = df['Close'] #close = open ??? data from Yahoo Finance

### Import risk free rate from 3-month TBill
data = pd.read_csv('Micro_FRED_3mo_TBill.csv', index_col='DATE')
riskfree = data[::-1] #reverse it for bookkeeping

### Import 10-year treasury yield
data = pd.read_csv('Micro_USTREASURY-YIELD.csv', index_col='Date')
teasure_yield = data[::-1] #reverse it for bookkeeping
tenyr = teasure_yield['10 YR']

# ### List of variables loaded:
# sp_change
# cpi
# exp_infl
# indpro
# realGDP
# dji_change
# rp
# nas
# nk_change
# r2000
# crude
# RMB
# usd2euro
# gold
# Yen
# bullish_senti
# yieldspread
# bse_change
# cac_change
# dax_change
# ftse_change
# hsi_change
# nyse_change
# sh_close
# riskfree
# tenyr

# added a sanity check variable (NJ temp)
#TODO: add the government debt/GDP ratio

# added P/E 10 Schiller CAPE (should be most relevant)
shiller = pd.read_csv('Shiller_CAPE.csv', index_col='Date', parse_dates=True)
pe10 = shiller['P/E 10 CAPE']

#TODO: add dividend yield
#TODO: add “consensus” real earnings growth, roughly measured by the trailing
# 36-month growth rate;
#TODO: add the other variable that combines the dividend yield with “trend” earnings growth,
# measured by the trailing 120-month growth rate.
#TODO: add Carry (Pedersen)


### TODO: Concat different data frames
ret=sp_change / sp_close #use return for percentage change in variables, use absolute change for absolute values
print ret.head()
#TODO: cut off at 2006-03-01 to 2016-01-01 to make sure data line up

#TODO: run regression (use single variable AND multi-var)
