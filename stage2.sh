#!/bin/sh
echo "April 1, 2016 - Spring 2016 IW Demonstration"
echo "=========================================================="
echo "=========================================================="
echo "I will show Stage 1 first..."
python stage1main.py Master_Color_Learning_0304.csv Master_Color_Test_0304.csv
echo "This is Stage 2 Demo..."
echo "First off, we have the simplest linear regression using a dummy variable as benchmark"
python stage2.py Master_Change_Learning_0304.csv Master_Change_Test_0304.csv
echo "=========================================================="
echo "=========================================================="
echo "Second, we will run what we have so far for integration"
python stage2main.py
# echo "=========================================================="
# echo "=========================================================="
# echo "Third, we will run Nystroem method and Fourier"
# python stage2ny.py Master_Change_Learning_0304.csv Master_Change_Test_0304.csv
echo "=========================================================="
# echo "=========================================================="
# echo "Next, we will run Genetic Algorithm"
# python stage2GA.py Master_Change_Learning_0304.csv Master_Change_Test_0304.csv
echo "======================== END ============================="
