# Main interface for stage2 demo March 22
# Eddie Chen '16, March 18, 2016


from sys import argv
import numpy as np
import pandas as pd
from sklearn import preprocessing
import stage2rfr
import stage2svr
import bucketing
import math
import stage2plot
import stage2GA
import stage2ny
"""
    Algos to use:
        stage2GA (for GA + SVR), [separate demo]
        stage2ny (for Nystroem / Fourier + SVR),
        stage2svr (kernel ridge + GridSearchCV) ,
        stage2rfr (Random Forest, ExtraTree, AdaBoost, GaussianProcess)
        stage2 (for simple multi-regression, with a dummy variable)
"""
# Step 1: read color prediction from stage 1

#filename = 'stage1res.csv'
filename = 'stage1res_bagging.csv'
color_training = 'Master_Color_Learning_0304.csv'
color_testName = 'Master_Color_Test_0304.csv'

color_predicted = np.genfromtxt(filename).astype('int')
training_bucket = np.genfromtxt(color_training, delimiter=',', autostrip=True, skip_header=1, usecols = [12]).astype('int')
color_valid = np.genfromtxt(color_testName, delimiter=',', autostrip=True, skip_header=1, usecols = [12]).astype('int')
#stage2plot.plot(color_predicted, color_valid)

v = np.unique(color_predicted)

#print training_bucket

change_training = 'Master_Change_Learning_0304.csv'
change_valid = 'Master_Change_Test_0304.csv'
data_training_master = np.genfromtxt(change_training, comments="#", delimiter=',', autostrip=True, skip_header=1)
data_valid_master = np.genfromtxt(change_valid, comments="#", delimiter=',', autostrip=True, skip_header=1)

# impute data for completeness
data_training_master = preprocessing.Imputer().fit_transform(data_training_master)
data_valid_master = preprocessing.Imputer().fit_transform(data_valid_master)


v = v.tolist()
label = 3 #FIXME: input label and xstart, they are now hard-coded
xstart = 4

# X_valid = data_valid_master[sel:sel+1, xstart:]
# y_valid = data_valid_master[sel:sel+1,label:label+1]
#
# print "================ Actual Value of the Day ==================="
# print "Actual value on this day, 30min into trading is %0.3f percent" % y_valid
# #print "The respective predicted bucket is", color_predicted[sel]
#
# X_train = data_training_master[0:1, xstart:]
# y_train = data_training_master[0:1, label:label+1]
# y_train = y_train.ravel()
# # Step 2Construct differen training and validation groups based on color
# for i in xrange(len(training_bucket)):
#     if (training_bucket[i] == color_predicted[sel]):
#         X_train = np.vstack((X_train, data_training_master[i:i+1, xstart:]))
#         y_train = np.vstack((y_train, data_training_master[i:i+1, label:label+1]))
# #print X_train.shape

for iter in v:
    # clear training and validation sets
    X_valid = data_valid_master[0:1, xstart:]
    y_valid = data_valid_master[0:1,label:label+1]

    X_train = data_training_master[0:1, xstart:]
    y_train = data_training_master[0:1, label:label+1]
    # Step 2: Construct differen training and validation groups based on color
    print "Bucket Value is %d" % iter
    for t in xrange(len(color_predicted)):
        if (color_predicted[t] == iter):
            sel = t;
            t_X_valid = data_valid_master[sel:sel+1, xstart:]
            t_y_valid = data_valid_master[sel:sel+1,label:label+1]
            X_valid = np.vstack((X_valid, t_X_valid))
            y_valid = np.vstack((y_valid, t_y_valid))
    for t in xrange(len(training_bucket)):
        if (training_bucket[t] == iter):
            sel = t;
            t_X_train = data_training_master[sel:sel+1, xstart:]
            t_y_train = data_training_master[sel:sel+1,label:label+1]
            X_train = np.vstack((X_train, t_X_train))
            y_train = np.vstack((y_train, t_y_train))
    #print X_train.shape
    ### standardize y variable
    scaler = preprocessing.StandardScaler().fit(y_train)
    y_train = scaler.transform(y_train)
    y_valid = scaler.transform(y_valid)
    data = np.column_stack((y_train,X_train))
    valid = np.column_stack((y_valid, X_valid))
    print data.shape
    # Step 3: feed into various algos for results
    # assuming days are aligned already

    print "The error terms below are based on standardized y data"
    res = scaler.inverse_transform(stage2rfr.rfr(X_train, y_train.ravel(), X_valid, y_valid))

    print ""
    y_valid_predict = scaler.inverse_transform(stage2rfr.extratree(X_train, y_train.ravel(), X_valid, y_valid))
    res = np.column_stack((res,y_valid_predict))
    print ""
    y_valid_predict = scaler.inverse_transform(stage2rfr.ada(X_train, y_train.ravel(), X_valid, y_valid))
    res = np.column_stack((res,y_valid_predict))
    print ""
    #print stage2rfr.gaussian(X_train, y_train.ravel(), X_valid, y_valid) #FIXME: error now
    #
    y_valid_predict = scaler.inverse_transform(stage2svr.svr_search(X_train, y_train, X_valid, y_valid))
    res = np.column_stack((res,y_valid_predict))
    print ""
    y_valid_predict = scaler.inverse_transform(stage2svr.kernel_ridge(X_train, y_train, X_valid, y_valid))
    res = np.column_stack((res,y_valid_predict))
    print ""
    print "Running GA wth linear regression fitness function..."
    y_valid_predict = scaler.inverse_transform(stage2GA.GA(data, valid))
    res = np.column_stack((res,y_valid_predict))
    print ""
    stage2ny.NY(X_train, y_train, X_valid, y_valid)
    print ""
    print "/-------------------------------------------------------/"
    print "Compiling these predictions into one single conclusion..."
    avg = bucketing.boosting_float(res)
    res = np.column_stack((res,avg))
    print "---Overall RMSE is %.4f" % (math.sqrt(np.mean((avg - y_valid) ** 2)))
    # plot results, if we want to:
    #stage2plot.plot(avg, y_valid)
    print ""
    print "...Saving results for Bucket Value of %d" % iter
    print ""
    res_name = 'BucketOf'+str(iter)+'stage2res_raw.csv'
    np.savetxt(res_name,res, delimiter=',')
    print "/*********************************************/"
    print ""
