### Eddie Chen '16, Mar 4
### main, pipeline
from sys import argv
import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import svm_lib
import svr_lib
import log_reg
import decisionTree
import bucketing
import echen_rbm #for RBM implementation
import math
# helper function to compare bagging and boosting(averaging)
def compare_bench(y, bench):
    sum = 0
    for i in xrange(len(y)):
        if (y[i] != bench[i]):
            sum = sum+1
    print("Hit ratio is %.3f" % (1.0 - sum/(len(y) * 1.0)))
    print("    RMSE is %.3f" % math.sqrt(np.mean((y - bench) ** 2)))
    return sum

def confusion_matrix_print(y, bench):
    cm = confusion_matrix(bench, y)
    np.set_printoptions(precision=2)
    print('Confusion matrix, without normalization')
    print(cm)

def plot_confusion_matrix(cm, target_names, title='Confusion matrix', cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(target_names))
    plt.xticks(tick_marks, target_names, rotation=45)
    plt.yticks(tick_marks, target_names)
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

print "============= Stage 1 Model, version 1.0 ===================="

# input dataset
print "Loading Data..."
# input dataset
if (len(argv) < 2):
    raise NameError('No filename specified')

# learning set
filename = argv[1]
data = np.genfromtxt(filename, comments="#", delimiter=',', autostrip=True, skip_header=1)
(height, width) = data.shape
print "data dimension is "+str(data.shape)
dd = pd.read_csv(filename)
print "for your reference, columns' name and index are as follows: "
for iter in range(len(dd.columns)):
    print(dd.columns[iter],iter)
print "============================================================="
# toggle column number
label = int(raw_input('discrete y variable column: '))
clabel = int(raw_input('continuous y variable column: '))
xstart = int(raw_input('X variable column start: '))
# label = 12
# clabel = 5
# xstart = 13

print "Selected discrete y as "+str(dd.columns[label])
print "Selected continuous y as "+str(dd.columns[clabel])
print "Selected x starts at "+str(dd.columns[xstart])

# test set
testName = argv[2]

# print('Learning set is ', filename)
#
# print('Validation set is ', testName)
valid = np.genfromtxt(testName, comments="#", delimiter=',', autostrip=True, skip_header=1)
res = valid[0:,label:label+1]
benchmark = res
# print "============================================================="
# neu = raw_input("Run Neural Network? Y/N: ")
# if (neu == 'Y'):
#     try:
#         res_neural = neural_classifier_scikit.classifier(filename, testName, label, xstart)
#         res_neural = bucketing.bucketing(res_neural, np.mean(res_neural), np.std(res_neural))
#         res = np.column_stack((res,res_neural))
#     except:
#         print "Error in running neural network; are you sure you have correctly formatted data?"
print "============================================================="
print("Running SVM...")

try:
    res_svm = svm_lib.svmlib(filename, testName, label, xstart)
    res = np.column_stack((res,res_svm))
except:
    print "-!-Error in running SVM; are you sure you have label data?"
# print "============================================================="
# print("Running SVR...")
# try:
#     # continuous y input, catch discrete output
#     res_svr = svr_lib.svr(filename, testName, clabel, xstart)
#     res = np.column_stack((res,res_svr))
# except:
#     print "-!-Error in running SVR; are you sure you have the correct data formats?"
print "============================================================="
print("Running Logistic Regression...")

try:
    res_log = log_reg.logreg(filename, testName, label, xstart)
    res_log = bucketing.bucketing(res_log, np.mean(res_log), np.std(res_log))
    res = np.column_stack((res,res_log))
    # SGDClassifier has poor accuracy
    res_sgd = log_reg.SGDClassifier(filename, testName, label, xstart)
    res = np.column_stack((res,res_sgd))
except:
    print "-!-Error in running log reg; are you sure you have the correct data formats?"
print "============================================================="
print("Running CART Decision Tree Classifier...")
try:
    res_cart = decisionTree.dt(filename, testName, label, xstart)
    res = np.column_stack((res,res_cart))
except:
    print "-!-Error in running CART; are you sure you have the correct data formats?"
# print "============================================================="

print "===================== Saving Results... =========================="
boost_res = bucketing.boosting(res)
bag_res = bucketing.bagging(res)

print "For averaging, we have confusion_matrix: "
confusion_matrix_print(boost_res, benchmark)
cm = confusion_matrix(benchmark, boost_res)
plt.figure()
target_names = np.unique(boost_res)
plot_confusion_matrix(cm, target_names)

print "For bagging, we have confusion_matrix: "
confusion_matrix_print(bag_res, benchmark)
cm = confusion_matrix(benchmark, bag_res)
plt.figure()
target_names = np.unique(bag_res)
plot_confusion_matrix(cm, target_names)
plt.show()

print "For averaging, we have: "
compare_bench(boost_res, benchmark)
print "For bagging, we have: "
compare_bench(bag_res, benchmark)

#print res
np.savetxt('stage1res_raw.csv',res, delimiter=',')
np.savetxt('stage1res_bagging.csv',bag_res, delimiter=',')
np.savetxt('stage1res_boosting.csv',boost_res, delimiter=',')
# print "===================== Beginning Stage 2... =========================="
# filename = raw_input("Please input Stage 2 Training Set name: ")
# testName = raw_input("Please input Stage 2 Validation Set name: ")
#
# #TODO: call stage2 algo with y_train, bag_res as input; construct new training and validation sets

print "================= End of Demo ==============================="
