# Eddie Chen, Feb 24
# first attempt to color days
# Simple 3-layer Neural Network to categorize days into different buckets
# Buckets are defined according to the SP_Change, Math.round(100*SP_Change),
# and then each discrete number gets a level (-4 to +4)

from sknn.mlp import Classifier, Layer
import pandas as pd
import numpy as np
import statsmodels.api as sm
from sys import argv
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler

# sigmoid function
def nonlin(x,deriv=False):
    if(deriv==True):
        return x*(1-x)
    return 1/(1+np.exp(-x))


# input dataset
if (len(argv) < 2):
    raise NameError('No filename specified')

filename = argv[1]
# filename = 'Master_Color_Learning_0224.csv'
#print filename
data = np.genfromtxt(filename, delimiter=',')
(height, width) = data.shape
dayClassCol = 7
spCol = 6
y = data[1:,spCol:spCol+1] #dayClass, output array
X = data[1:,dayClassCol+1:width-1] #input
#scale data
# pipeline = Pipeline([
#         ('min/max scaler', MinMaxScaler(feature_range=(0.0, 1.0))),
#         ('neural network', Classifier(layers=[Layer("Linear")], n_iter=25))])
# pipeline.fit(X, y)

# print type(X)
#
# below taken from: http://iamtrask.github.io/2015/07/12/basic-python-network/
# seed random numbers to make calculation
# deterministic (just a good practice)
np.random.seed(1)
print X.shape
# initialize weights randomly with mean 0
syn0 = 2*np.random.random((23,height)) - 1
syn1 = 2*np.random.random((height,1)) - 1

for iter in xrange(40000):

    # forward propagation
    # Feed forward through layers 0, 1, and 2
    l0 = X
    l1 = nonlin(np.dot(l0,syn0))
    l2 = nonlin(np.dot(l1,syn1))

    # how much did we miss?
    # l1_error = y - l1
    l2_error = y - l2
    if (iter % 10000) == 0:
        print "Error:" + str(np.mean(np.abs(l2_error)))

    # in what direction is the target value?
    # were we really sure? if so, don't change too much.
    l2_delta = l2_error*nonlin(l2,deriv=True)

    # how much did each l1 value contribute to the l2 error (according to the weights)?
    l1_error = l2_delta.dot(syn1.T)

    # in what direction is the target l1?
    # were we really sure? if so, don't change too much.

    l1_delta = l1_error * nonlin(l1,deriv=True)
    # update weights
    syn1 += l1.T.dot(l2_delta)
    syn0 += l0.T.dot(l1_delta)

print "Output After Training:"
print l2
