# this is neural network classifier
# Eddie Chen, Mar 25, 2016

#not available in scikit 0.17

from sklearn.neural_network import MLPClassifier
import numpy as np
import math

def classifier(filename, testName, label, xstart):
    print "Neural Network Classifier Demo, version 1.0"
    # input dataset
    # MLP trains using Backpropagation
    if (filename == ""):
        raise NameError('No filename specified')

    print "Loading Data..."

    data = np.genfromtxt(filename, comments="#", delimiter=',', autostrip=True, skip_header=1)
    (height, width) = data.shape
    print "data dimension is "+str(data.shape)

    y_train = data[0:,label:label+1] #target array
    X_train = data[0:,xstart:width] #input

    data = np.genfromtxt(testName, comments="#", delimiter=',', autostrip=True, skip_header=1)
    (height, width) = data.shape

    y_valid = data[0:,label:label+1] #dayClass, output array
    X_valid = data[0:,xstart:width] #input

    print "Setting up neural network classifier..."
    clf = MLPClassifier(algorithm='l-bfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1)
    clf.fit(X_train, y_train.ravel())
    y_valid_predict = clf.predict(X_valid)
    rmse = math.sqrt(np.mean((y_valid_predict - y_valid) ** 2))
    print "RMSE is %.3f" % rmse
    return y_valid_predict


#test main
filename = 'Master_Color_Learning_0304.csv'
testName = 'Master_Color_Test_0304.csv'
label = 12
xstart = 15
classifier(filename, testName, label, xstart)
