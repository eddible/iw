# Stage II, Nystroem method
# kernel trick

# Eddie Chen '16, March 13, 2016
# basis: http://peekaboo-vision.blogspot.com/2012/12/kernel-approximations-for-efficient.html
# http://scikit-learn.org/dev/auto_examples/plot_kernel_approximation.html
"""
    References:
    Williams, C.K.I. and Seeger, M. "Using the Nystroem method to speed up
    kernel machines", Advances in neural information processing systems 2001
    T. Yang, Y. Li, M. Mahdavi, R. Jin and Z. Zhou "Nystroem Method vs Random
    Fourier Features: A Theoretical and Empirical Comparison",
    Advances in Neural Information Processing Systems 2012
"""
"""
    Using Nystroem method, Approximate a kernel map using a subset of the training data.
    Constructs an approximate feature map for an arbitrary kernel using a subset of the data as basis.
"""

import numpy as np
from time import time
import math
from sys import argv
# Import classifiers and performance metrics
from sklearn import svm, pipeline, preprocessing
from sklearn.kernel_approximation import (RBFSampler,
                                          Nystroem)
from sklearn.decomposition import PCA

def NY(X_train, y_train, X_valid, y_valid):
    """
        Main, TODO: automate parameter input
    """
    # # input dataset
    # print "Loading Data..."
    # # input dataset
    # if (len(argv) < 2):
    #     raise NameError('No filename/not enough filenames specified')
    #
    # filename = argv[1]
    # data = np.genfromtxt(filename, comments="#", delimiter=',', autostrip=True, skip_header=1)
    # (height, width) = data.shape
    # print "data dimension is "+str(data.shape)
    # # dd = pd.read_csv(filename)
    # # print "for your reference, columns' name and index are as follows: "
    # # for iter in range(len(dd.columns)):
    # #     print(dd.columns[iter],iter)
    # # toggle column number
    # # label = int(raw_input('y variable column: '))
    # # xstart = int(raw_input('X variable column start: '))
    # #
    # # print "Selected y as "+str(label)
    # # print "Selected x starts at "+str(xstart)
    # label = 3
    # xstart = 4;
    # # test set
    # testName = argv[2]
    # valid = np.genfromtxt(testName, comments="#", delimiter=',', autostrip=True, skip_header=1)
    # #
    # # print('Learning set is ', filename)
    # #
    # # print('Validation set is ', testName)
    # size = width - xstart
    #
    # # standardize data
    # data = preprocessing.Imputer().fit_transform(data)
    # valid = preprocessing.Imputer().fit_transform(valid)
    # scaler = preprocessing.StandardScaler().fit(data)
    # data = scaler.transform(data)
    # valid = scaler.transform(valid)
    # # extract X and y
    # y_train = data[0:,label:label+1]
    # X_train = data[0:,xstart:width] #input
    #
    # y_valid = valid[0:,label:label+1]
    # X_valid = valid[0:,xstart:width] #input

    print "Running Nystroem Method and Fourier Method"
    print "training X dataset dimension is", X_train.shape

    # Create a regressor: a support vector regressor
    kernel_svr = svm.SVR(gamma=.2) #rbf kernel
    linear_svr = svm.LinearSVR()

    # create pipeline from kernel approximation
    # and linear svr
    feature_map_fourier = RBFSampler(gamma=.2, random_state=1)
    feature_map_nystroem = Nystroem(gamma=.2, random_state=1)
    fourier_approx_svr = pipeline.Pipeline([("feature_map", feature_map_fourier),
                                            ("svr", svm.LinearSVR())])

    nystroem_approx_svr = pipeline.Pipeline([("feature_map", feature_map_nystroem),
                                            ("svr", svm.LinearSVR())])

    # fit and predict using linear and kernel svm:
    print "Results before kernel approximation:"
    kernel_svr_time = time()
    kernel_svr.fit(X_train, y_train.ravel())
    kernel_svr_score = kernel_svr.score(X_valid, y_valid)
    kernel_svr_time = time() - kernel_svr_time
    kernel_predict = kernel_svr.predict(X_valid)
    print("kernel svr takes %.3fs" % kernel_svr_time)
    print("kernel svr score is %.3f" % kernel_svr_score)
    print("kernel svr has RMSE of %.3f" % math.sqrt(np.mean((kernel_predict - y_valid) ** 2)))

    linear_svr_time = time()
    linear_svr.fit(X_train, y_train.ravel())
    linear_svr_score = linear_svr.score(X_valid, y_valid.ravel())
    linear_svr_time = time() - linear_svr_time
    linear_predict = linear_svr.predict(X_valid)
    print("linear svr takes %.3fs" % linear_svr_time)
    print("linear svr score is %.3f" % linear_svr_score)
    print("linear svr has RMSE of %.3f" % math.sqrt(np.mean((linear_predict - y_valid) ** 2)))

    print "########################################################################"
    print "Kernel Approximation using Fourier Transformation and Nystriem Method:"
    # now, use kernel approximation
    sample_sizes = 30 * np.arange(10, 25) #gives me 300 to 750

    fourier_scores = []
    nystroem_scores = []
    fourier_times = []
    nystroem_times = []

    for D in sample_sizes:
        print "---for sample size %d" % D
        fourier_approx_svr.set_params(feature_map__n_components=D)
        nystroem_approx_svr.set_params(feature_map__n_components=D)
        start = time()
        nystroem_approx_svr.fit(X_train, y_train.ravel())
        nystroem_predict = nystroem_approx_svr.predict(X_valid)
        nystroem_times.append(time() - start)

        start = time()
        fourier_approx_svr.fit(X_train, y_train.ravel())
        fourier_predict = fourier_approx_svr.predict(X_valid)
        fourier_times.append(time() - start)

        fourier_score = fourier_approx_svr.score(X_valid, y_valid.ravel())
        nystroem_score = nystroem_approx_svr.score(X_valid, y_valid.ravel())
        print("Fourier score is %.3f" % fourier_score)
        print("Fourier approximation with %d Sample Size has RMSE of %.3f" % (D, math.sqrt(np.mean((fourier_predict - y_valid) ** 2))))
        print("Nystroem score is %.3f" % nystroem_score)
        print("Nystroem approximation with %d Sample Size has RMSE of %.3f" % (D, math.sqrt(np.mean((nystroem_predict - y_valid) ** 2))))
        nystroem_scores.append(nystroem_score)
        fourier_scores.append(fourier_score)

###############################################################################
# # plot the results:
# plt.figure(figsize=(8, 8))
# accuracy = plt.subplot(211)
# # second y axis for timeings
# timescale = plt.subplot(212)
#
# accuracy.plot(sample_sizes, nystroem_scores, label="Nystroem approx. kernel")
# timescale.plot(sample_sizes, nystroem_times, '--',
#                label='Nystroem approx. kernel')
#
# accuracy.plot(sample_sizes, fourier_scores, label="Fourier approx. kernel")
# timescale.plot(sample_sizes, fourier_times, '--',
#                label='Fourier approx. kernel')
#
# # horizontal lines for exact rbf and linear kernels:
# accuracy.plot([sample_sizes[0], sample_sizes[-1]],
#               [linear_svr_score, linear_svr_score], label="linear svr")
# timescale.plot([sample_sizes[0], sample_sizes[-1]],
#                [linear_svr_time, linear_svr_time], '--', label='linear svr')
#
# accuracy.plot([sample_sizes[0], sample_sizes[-1]],
#               [kernel_svr_score, kernel_svr_score], label="rbf svr")
# timescale.plot([sample_sizes[0], sample_sizes[-1]],
#                [kernel_svr_time, kernel_svr_time], '--', label='rbf svr')
#
# # vertical line for dataset dimensionality = 23
# accuracy.plot([23, 23], [0.7, 1], label="n_features")
#
# # legends and labels
# accuracy.set_title("Classification accuracy")
# timescale.set_title("Training times")
# accuracy.set_xlim(sample_sizes[0], sample_sizes[-1])
# accuracy.set_xticks(())
# accuracy.set_ylim(np.min(fourier_scores), 1)
# timescale.set_xlabel("Sampling steps = transformed feature dimension")
# accuracy.set_ylabel("Classification accuracy")
# timescale.set_ylabel("Training time in seconds")
# accuracy.legend(loc='best')
# timescale.legend(loc='best')
