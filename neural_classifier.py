# Eddie Chen '16, Neural Network to predict color, Classifier based on dayClass
# input two files, output predictions for dayClass

# this is the classifier
# TODO: not working yet; fix sknn???

from sknn.mlp import Classifier, Layer
import numpy as np
from sys import argv
from sklearn import preprocessing
import math
import logging
logging.basicConfig()

#reference: http://scikit-neuralnetwork.readthedocs.org/en/latest/module_mlp.html#regressor

# input dataset
if (len(argv) < 2):
    raise NameError('No filename specified')

print "Loading Data..."

# learning set
filename = argv[1]
data = np.genfromtxt(filename, comments="#", delimiter=',', autostrip=True, skip_header=1)
(height, width) = data.shape

dayClassCol = 6 # use SP_Change
spCol = 5
y_train = data[0:,dayClassCol:dayClassCol+1] #dayClass, output array
X_train = data[0:,dayClassCol+1:width] #input

# test set
testName = argv[2]
data = np.genfromtxt(testName, comments="#", delimiter=',', autostrip=True, skip_header=1)
(height, width) = data.shape

y_valid = data[0:,dayClassCol:dayClassCol+1] #dayClass, output array
X_valid = data[0:,dayClassCol+1:width] #input

print "Setting up neural network..."

#scale data # doesn't work yet
# pipeline = Pipeline([
#         ('min/max scaler', MinMaxScaler(feature_range=(0.0, 1.0))),
#         ('neural network', Classifier(layers=[Layer("Linear")], n_iter=25))])
# pipeline.fit(X_train, y_train)
scaler = preprocessing.StandardScaler().fit(X_train)
X_train = scaler.transform(X_train)
# scaler = preprocessing.StandardScaler().fit(y_train)
# y_train = scaler.transform(y_train)
scaler = preprocessing.StandardScaler().fit(X_valid)
X_valid = scaler.transform(X_valid)
# scaler = preprocessing.StandardScaler().fit(y_valid)
# y_valid = scaler.transform(y_valid)


nn = Classifier(
    layers=[
        Layer("Sigmoid", units=100),
        Layer("Softmax")],
    learning_rate=0.001,
    n_iter=25)

print "Running neural network..."
nn.fit(X_train, y_train)

y_valid_predict = nn.predict(X_valid)

print "Done, calculating errors..."

sum = 0

#calculate error, counting miss-matches
for i in xrange(len(y_valid)):
    sum = sum + abs(y_valid[i] - y_valid_predict[i])

avg = sum / len(y_valid)

print "Error Percentage is: "
print str(avg*100)+"%"

# output new values
outname = "predicted_Classifier_"+filename
#outArray = np.concatenate((y_valid_predict,y_valid),axis=0)
# print outname

np.savetxt(outname, y_valid_predict)