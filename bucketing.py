# stage 1, pre-processing returns to discrete buckets
# Eddie Chen '16, Mar 15
import math
import numpy as np
from sys import argv
import pandas as pd
from sklearn import preprocessing

#TODO: how do we bucket the validation data set?
#currently, use training mean and std

def bagging_float(input):
    print "bagging for float is not implemented yet"

def boosting_float(input):
    """
        find the predominant sign first, then calculate avg for the same sign
    """
    (h,w) = input.shape
    res = np.average(input, axis=1)
    input = input.tolist()
    for i in xrange(h):
        pos = 0
        pos_sum = 0.0
        neg = 0
        neg_sum = 0.0
        for j in xrange(w):
            if (input[i][j] > 0.0):
                pos = pos+1
                pos_sum = pos_sum+input[i][j]
            else:
                neg=neg+1
                neg_sum = neg_sum + input[i][j]
        if (pos > neg):
            res[i] = pos_sum / pos
        else:
            res[i] = neg_sum / neg
    return res


def bucketing(y, mean, std):
    """
        a method to get buckets for a continuous input, standardize the input
        and then put it into buckets representing how far (how many standard
        deviations away) it is from center. Output a discrete int array
    """
    res = y
    for i in xrange(len(y)):
        res[i] = int(round((y[i]-mean)/(std*1.0)))
        if (res[i] > 1):
            res[i] = 2
        if (res[i] < -1):
            res[i] = -2

    return res.astype(int)

def bagging(input):
    """
        a method to take in a ndarray of bucketing results,
        output a resultant array with bagging method: for each row, cells vote
        for the most common element and that is the result.
        int.
    """
    #TODO: cast from float64 to int64
    (h, w) = input.shape
    res = np.average(input, axis=1)
    for i in xrange(h):
        t = input[i].astype(int)
        (temp, count) = np.unique(t, return_counts =True)
        res[i] = temp[count.argmax(axis=0)]
    return res

def boosting(input):
    """
        a method to take in a ndarray of bucketing results,
        output a resultant array with boosting method: take average for that row
    """
    (h, w) = input.shape
    res = np.average(input, axis=1) #finding average for each row
    for i in xrange(len(input)):
        res[i] = int(round(res[i]))
    return res


### Test Main
def main(filename, testName):
    data = np.genfromtxt(filename, comments="#", delimiter=',', autostrip=True, skip_header=1)
    (height, width) = data.shape
    print "data dimension is "+str(data.shape)
    dd = pd.read_csv(filename)
    print "for your reference, columns' name and index are as follows: "
    for iter in range(len(dd.columns)):
        print(dd.columns[iter],iter)
    # toggle column number
    label = int(raw_input('y variable column: '))
    xstart = int(raw_input('X variable column start: '))

    print "Selected y as "+str(label)
    print "Selected x starts at "+str(xstart)
    # test set
    valid = np.genfromtxt(testName, comments="#", delimiter=',', autostrip=True, skip_header=1)

    # standardize data
    data = preprocessing.Imputer().fit_transform(data)
    valid = preprocessing.Imputer().fit_transform(valid)
    # extract X and y
    y_train = data[0:,label:label+1]
    X_train = data[0:,xstart:width] #input

    y_valid = valid[0:,label:label+1]
    X_valid = valid[0:,xstart:width] #input

    mean = y_train.mean()
    std = np.std(y_train)
    print bucketing(y_train, mean, std)
    print bucketing(y_valid, mean, std)


def test_main():
    """
        Test Main
    """
    # # input dataset
    # print "Loading Data..."
    # # input dataset
    # if (len(argv) < 2):
    #     raise NameError('No filename/not enough filenames specified')
    # filename = argv[1]
    # testName = argv[2]
    #main(filename, testName)

    data = np.array([[-0.1,-0.1,-0.11,-0.01,-0.1],[-1.2,0.1,1.2,1.1,1.2],[-1.1,1.2,0.2,0.2,0.1]])
    print data
    print boosting_float(data)

#test_main()
