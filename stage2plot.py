#plot y_pred with y_valid
from matplotlib import pyplot as pl
import numpy as np
import math
from sklearn.gaussian_process import GaussianProcess

def plot(y_pred, y):
    # Plot the function, the prediction and the 95% confidence interval based on
    # the MSE
    sigma = math.sqrt(np.mean((y_pred - y) ** 2))
    x = np.atleast_2d(np.linspace(0, 10, len(y_pred))).T
    fig = pl.figure()
    pl.plot(x, y, 'r.', markersize=10, label=u'Observations')
    pl.plot(x, y_pred, 'b-', label=u'Prediction')
    pl.fill(np.concatenate([x, x[::-1]]),
            np.concatenate([y_pred - 1.9600 * sigma,
                           (y_pred + 1.9600 * sigma)[::-1]]),
            alpha=.5, fc='b', ec='None', label='95% confidence interval')
    pl.xlabel('$x$')
    pl.ylabel('$f(x)$')
    pl.ylim(-10, 20)
    pl.legend(loc='upper left')
    pl.show()
    #pl.savefig(transparent=True)
